// range slider
const slider = document.querySelector('.slider_container');
if(slider){
  $( '.slider_container input' ).on( 'input', function( ) {
    $( this ).css( 'background', 'linear-gradient(to right, #C0D271 0%, #C0D271 '+this.value +'%, #D8D8D8 ' + this.value + '%, #D8D8D8 100%)' );
    console.log(this.value);
  } );

}

//  toggle password vissibility
let input_field = $('#password_input') ;
let input_span = $('.span_absolute') ;
$(document).ready(()=>{
  input_span.click(()=>{
    if(input_field.val() != ""){
      if(input_span.hasClass('hidden_span')){
        input_span.removeClass('hidden_span');
        input_field.attr('type', 'text')
      }else{
        input_field.attr('type', 'password')
        input_span.addClass('hidden_span');
  
      }
    }
  })

  
});

// scrollspy Functions
const scrollspyer = document.querySelector('.about_body_container');
if(scrollspyer){
  function getSections($links) {
    return $(
      $links
        .map((i, el) => $(el).attr('href'))
        .toArray()
        .filter(href => href.charAt(0) === '#')
        .join(','),
  
    );
  }
  
  function activateLink($sections, $links) {
    const yPosition = $window.scrollTop() + 110;
    // console.log(yPosition);
  
    for (let i = $sections.length - 1; i >= 0; i -= 1) {
      const $section = $sections.eq(i);
  
      if (yPosition >= $section.offset().top) {
        return $links
          .removeClass('active')
          .filter(`[href="#${$section.attr('id')}"]`)
          .addClass('active');
      }
    }
  }
  
  function onScrollHandler() {
    activateLink($sections, $links);
  }
  
  function onClickHandler(e) {
    const href = $.attr(e.target, 'href');
  
    e.preventDefault();
    $root.animate(
      { scrollTop: $(href).offset().top },
      500,
      () => (window.location.hash = href),
    );
  
    return false;
  }
  
  // Variables
  const $window = $(window);
  const $links = $('.menu_spy > li > a');
  const $sections = getSections($links);
  const $root = $('html, body');
  const $hashLinks = $('a[href^="#"]:not([href="#"])');
  
  // Events
  $window.on('scroll', onScrollHandler);
  $hashLinks.on('click', onClickHandler);
  
  // Body
  activateLink($sections, $links);
  
}

// $(function(){
  const sponsorship_chech = $(".sponsorship_check")

  // toggle name edit on sponsorship savings 
  if(sponsorship_chech){
    // $(".view_detail").click((e, index)=>{
    //   $(e.target).fadeOut();
    //   console.log(index);
    // })
    let allContainers = document.querySelectorAll(".product_body");
    $(allContainers).click(function(){
       $(this).find("div.view_detail").fadeOut(1000);
       $(this).find("div.hidden_element").fadeIn(500);
    })
  }
//view details of sponsorships
if(sponsorship_chech){
  $(".toggler").click((e)=>{
    $(e.target).parent(".toggle_name").toggleClass("toggle_name_class");
  })
}
  // Remove svg.radial-progress .complete inline styling
  if(sponsorship_chech){
    $('svg.radial-progress').each(function( index, value ) { 
      $(this).find($('circle.complete')).removeAttr( 'style' );
  });

  // Activate progress animation on scroll
  $(window).scroll(function(){
      $('svg.radial-progress').each(function( index, value ) { 
          // If svg.radial-progress is approximately 25% vertically into the window when scrolling from the top or the bottom
          if ( 
              $(window).scrollTop() > $(this).offset().top - ($(window).height() * 0.75) &&
              $(window).scrollTop() < $(this).offset().top + $(this).height() - ($(window).height() * 0.25)
          ) {
              // Get percentage of progress
              percent = $(value).data('percentage');
              // Get radius of the svg's circle.complete
              radius = $(this).find($('circle.complete')).attr('r');
              // Get circumference (2πr)
              circumference = 2 * Math.PI * radius;
              // Get stroke-dashoffset value based on the percentage of the circumference
              strokeDashOffset = circumference - ((percent * circumference) / 100);
              // Transition progress for 1.25 seconds
              $(this).find($('circle.complete')).animate({'stroke-dashoffset': strokeDashOffset}, 1250);
          }
      });
  }).trigger('scroll');
  }
  

// });
// profile menu on mobile
$("#burger").click(()=> {
  $("#add_class_on_mobile").removeClass("hide_class");
  $(".dash_overlay").addClass("active");
  $("#add_class_on_mobile").addClass("slideInLeft");
  $("#activate").hide();

});
$(".dash_overlay").click(()=> {
  $("#add_class_on_mobile").removeClass("slideInLeft");
  $(".dash_overlay").removeClass("active");
  $("#add_class_on_mobile").addClass("slideInRight ");
  $("#activate").show();
  setTimeout(()=> {
    $("#add_class_on_mobile").removeClass("slideInLeft ");
  }, 600);
});

// transaction history chart 
let callChart = document.querySelector("#transaction");

if(callChart){
  var options = {
    series: [4, 1, 6],
    labels: ["Wallet Top-ups", "Payouts", "Withdrawals"],
    colors:['#54AB68', '#0077B5', '#D8602C'],
    chart: {
    type: 'donut',
    width: 180
  
  },
  legend: {
    show:false,
  },
  dataLabels: {
    enabled: false,
  },
  };
  
  var chart = new ApexCharts(callChart, options);
  chart.render();
}

 // switch tab to select
 const changeTab = document.getElementById('mySelect');
 if(changeTab){
  $(changeTab).on('change', function (e) {
    $('#nav-tab a').eq($(this).val()).tab('show');
  });
 }

