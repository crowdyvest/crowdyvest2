@extends('layouts.auth-layout')

@section('content')
    <section class="auth_container">
        <form action="">
            <h4>Reset your password</h4>
            
            <div class="form_group pass_relative">
                <input type="email" placeholder="Email">
                <span class="aboslute_span">Email</span>

            </div>
            <button class="create_acct">Send Reset Password</button>
            <p class="altp forgot_pass">Don’t have an account? <a href="sign_up.html">Create account</a></p>
        </form>
    </section>
    @endsection