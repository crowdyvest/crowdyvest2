
@extends('layouts.app-layout')

@section('content')
    <section class="dash_body">
        <h3 class="dash_main_title see_all">Sponsorship Opportunities</h3>
        <div class="dashboard_container">
            <div class="opportunities">
                <div class="opportunities_grid">
                    @foreach($projects as $project)
                    <a href="{{ route('projectdetails', $project->slug) }}" class="opportunities_item">
                        <div class="img_container">
                            <img src="storage/farmimages/{{ $project->image }}" class="project_img" alt="Sponsorship opportunity">
                        </div>
                        <div class="product_body">
                            <h5 class="product_name">{{ $project->project_type->name }}</h5>
                            <img src="storage/img/logo/{{ $project->company->logo }}" alt="Owner's Logo">
                            <div class="returns">
                                <span>{{ $project->roi->percentage }}% returns</span>
                                <span>
                                        <svg width="4" height="4" viewBox="0 0 4 4" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <circle cx="2" cy="2" r="2" fill="#293737"/>
                                        </svg>
                                    </span>
                                <span>{{ $project->cycle }} months</span>
                            </div>
                            <div class="price">
                                <span>Starting from ₦{{ number_format($project->price_per_unit) }}</span>
                            </div>
                            <div class="sponsor_btn">
                                <span>{{ $project->status->name }}</span>
                            </div>
                        </div>
                    </a>
                    @endforeach
                </div>
                @if($show_pagination)
                <div class="pt-3 col-12 farm_pagination sponsorshop_pagination">
                {{ $projects->onEachSide(2)->links() }}
                </div>
                @endif
            </div>
        </div>
    </section>
@endsection
