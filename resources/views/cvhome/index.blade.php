@extends('layouts.apps')

@section('content')
    <section class="hero_bg">
        <div class="section_container">
            <div class="hero_content">
                <h2>Earn more money doing good.</h2>
                <p>Elementum, tristique turpis eget lorem eleifend odio eget morbi.</p>
            </div>
            <div class="tab_container">
                <nav class="slider_tab">
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-sponsor-tab" data-toggle="tab" href="#nav-sponsor"
                            role="tab" aria-controls="nav-sponsor" aria-selected="true">Sponsor</a>
                        <a class="nav-item nav-link" id="nav-save-tab" data-toggle="tab" href="#nav-save" role="tab"
                            aria-controls="nav-save" aria-selected="false">Save</a>
                        <a class="nav-item nav-link" id="nav-sponsorshop-tab" data-toggle="tab" href="#nav-sponsorshop"
                            role="tab" aria-controls="nav-sponsorshop" aria-selected="false">Sponsorshop</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-sponsor" role="tabpanel"
                        aria-labelledby="nav-sponsor-tab">
                        <form class="tab_content_container">
                            <div class=" right_border">
                                <div class="starting_amount">
                                    <span class="amount_words">Starting Amount</span>
                                    <div class="amt_container">
                                        <label class="amount_figure">₦</label>
                                        <input class="number_input" type="number" id="start_amt" value="250000">
                                    </div>
                                </div>
                                <div class="slider_container">
                                    <input type="range" value="50">
                                </div>
                            </div>
                            <div class=" right_border">
                                <div class="starting_amount">
                                    <span class="amount_words">Tenor</span>
                                    <div class="amt_container">
                                        <input class="number_input tenor_input" type="number" id="start__amt"
                                            value="12">
                                        <label class="amount_figure">months</label>
                                    </div>
                                </div>
                                <div class="slider_container">
                                    <input type="range" value="50">
                                </div>
                            </div>
                            <div class="end_balance">
                                <div class="starting_amount hide_element">
                                    <span class="amount_words">End Balance</span>
                                </div>
                                <div class="submit_btn">
                                    <span class="end_balance_">End Balance</span>

                                    <span>₦950,000</span>
                                    <button>Sponsor</button>
                                </div>
                            </div>
                        </form>

                    </div>
                    <div class="tab-pane fade" id="nav-save" role="tabpanel" aria-labelledby="nav-save-tab">
                        <form class="tab_content_container">
                            <div class=" right_border">
                                <div class="starting_amount">
                                    <span class="amount_words">Starting Amount</span>
                                    <div class="amt_container">
                                        <label class="amount_figure">₦</label>
                                        <input class="number_input" type="number" id="start___amt" value="250000">
                                    </div>
                                </div>
                                <div class="slider_container">
                                    <input type="range" value="50">
                                </div>
                            </div>
                            <div class=" right_border">
                                <div class="starting_amount">
                                    <span class="amount_words">Tenor</span>
                                    <div class="amt_container">
                                        <input class="number_input tenor_input" type="number" id="_start___amt"
                                            value="12">
                                        <label class="amount_figure">months</label>
                                    </div>
                                </div>
                                <div class="slider_container">
                                    <input type="range" value="50">
                                </div>
                            </div>
                            <div class="end_balance">
                                <div class="starting_amount hide_element">
                                    <span class="amount_words">End Balance</span>
                                </div>
                                <div class="submit_btn">
                                    <span class="end_balance_">End Balance</span>

                                    <span>₦950,000</span>
                                    <button>Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="nav-sponsorshop" role="tabpanel"
                        aria-labelledby="nav-sponsorshop-tab">
                        <form class="tab_content_container">
                            <div class=" right_border">
                                <div class="starting_amount">
                                    <span class="amount_words">Starting Amount</span>
                                    <div class="amt_container">
                                        <label class="amount_figure">₦</label>
                                        <input class="number_input" type="number" id="_start_amt" value="250000">
                                    </div>
                                </div>
                                <div class="slider_container">
                                    <input type="range" value="50">
                                </div>
                            </div>
                            <div class=" right_border">
                                <div class="starting_amount">
                                    <span class="amount_words">Tenor</span>
                                    <div class="amt_container">
                                        <input class="number_input tenor_input" type="number" id="__start_amt"
                                            value="12">
                                        <label class="amount_figure">months</label>
                                    </div>
                                </div>
                                <div class="slider_container">
                                    <input type="range" value="50">
                                </div>
                            </div>
                            <div class="end_balance">
                                <div class="starting_amount">
                                    <span class="amount_words">End Balance</span>
                                </div>
                                <div class="submit_btn">
                                    <span>₦950,000</span>
                                    <button>Sponsor</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <section class=" why_section">
        <div class="section_container">
            <h2 class="section_title">Why Crowdyvest?</h2>
            <div class="why_cv">
                <div class="why_item">
                    <div class="svg_container">
                        <svg width="72" height="72" viewBox="0 0 72 72" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M51 54C51 50.0218 49.4196 46.2064 46.6066 43.3934C43.7936 40.5804 39.9782 39 36 39C32.0218 39 28.2064 40.5804 25.3934 43.3934C22.5804 46.2064 21 50.0218 21 54"
                                stroke="#54AB68" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            <path d="M36 6V27" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M12.6602 30.66L16.9202 34.92" stroke="#54AB68" stroke-width="2"
                                stroke-linecap="round" stroke-linejoin="round" />
                            <path d="M3 54H9" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M63 54H69" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M55.0801 34.92L59.3401 30.66" stroke="#54AB68" stroke-width="2"
                                stroke-linecap="round" stroke-linejoin="round" />
                            <path d="M69 66H3" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M24 18L36 6L48 18" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                        </svg>
                    </div>
                    <p>Are you wondering how better financial decisions might affect your job or your life?</p>
                </div>
                <div class="why_item">
                    <div class="svg_container">
                        <svg width="72" height="72" viewBox="0 0 72 72" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M36 6V18" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M36 54V66" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M14.79 14.79L23.28 23.28" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M48.7197 48.72L57.2097 57.21" stroke="#54AB68" stroke-width="2"
                                stroke-linecap="round" stroke-linejoin="round" />
                            <path d="M6 36H18" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M54 36H66" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M14.79 57.21L23.28 48.72" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M48.7197 23.28L57.2097 14.79" stroke="#54AB68" stroke-width="2"
                                stroke-linecap="round" stroke-linejoin="round" />
                        </svg>
                    </div>
                    <p>Do you want to learn more about what Investing really means — and how to go about it?</p>
                </div>
                <div class="why_item">
                    <div class="svg_container">
                        <svg width="72" height="72" viewBox="0 0 72 72" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M36 66C52.5685 66 66 52.5685 66 36C66 19.4315 52.5685 6 36 6C19.4315 6 6 19.4315 6 36C6 52.5685 19.4315 66 36 66Z"
                                stroke="#54AB68" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            <path d="M66 36H54" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M18 36H6" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M36 18V6" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M36 66V54" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                        </svg>

                    </div>
                    <p>Do you want to understand how Wealth Creation is possible within your circumstances?</p>
                </div>
            </div>
        </div>
    </section>
    <section class="product_section">
        <div class="section_container">
            <div class="section_title">
                <h2 class="">Grow your money</h2>
                <p>Get started, whether you're investing, saving or seeking funds to grow your business.</p>
            </div>

            <div class="product_container">
                <div class="product_item">
                    <div class="product_body">
                        <p class="product_name">Sponsor</p>
                        <h4 class="prod_title">Earn more doing good</h4>
                        <p>Grow your funds by sponsoring high impact causes... any time, any day.</p>
                        <a href="#">Sponsor Now</a>
                    </div>
                </div>
                <div class="product_item">
                    <div class="product_body">
                        <p class="product_name">Save</p>
                        <h4 class="prod_title">Decent interest Savings</h4>
                        <p>Save smarter towards life goals with interests better than the banks.</p>
                        <a href="#">Start Saving</a>
                    </div>
                </div>
                <div class="product_item">
                    <div class="product_body">
                        <p class="product_name">Sponsorshop</p>
                        <h4 class="prod_title">Project-based Sponsorships</h4>
                        <p>Build your wealth sponsoring one impact project at a time.</p>
                        <a href="#">Sponsor Now</a>
                    </div>
                </div>
                <div class="product_item">
                    <div class="product_body">
                        <p class="product_name">B2B Loan</p>
                        <h4 class="prod_title">Grow your Business</h4>
                        <p>Keep your business moving with a funding solution to match.</p>
                        <a href="#">Apply Now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="simplicity">
        <div class="img_container">
            <img src="storage/img/team.jpg" alt="Team members">
        </div>
        <div class="body_bg">
            <div class="how_simple">
                <div class="how_simple_item">
                    <div class="_svg">
                        <svg width="72" height="72" viewBox="0 0 72 72" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M51 54C51 50.0218 49.4196 46.2064 46.6066 43.3934C43.7936 40.5804 39.9782 39 36 39C32.0218 39 28.2064 40.5804 25.3934 43.3934C22.5804 46.2064 21 50.0218 21 54"
                                stroke="#54AB68" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            <path d="M36 6V27" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M12.6602 30.66L16.9202 34.92" stroke="#54AB68" stroke-width="2"
                                stroke-linecap="round" stroke-linejoin="round" />
                            <path d="M3 54H9" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M63 54H69" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M55.0801 34.92L59.3401 30.66" stroke="#54AB68" stroke-width="2"
                                stroke-linecap="round" stroke-linejoin="round" />
                            <path d="M69 66H3" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M24 18L36 6L48 18" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                        </svg>
                    </div>
                    <div class="_body">
                        <h4>Simple</h4>
                        <p>Are you wondering how better financial decisions might affect your job or your life?</p>
                    </div>
                </div>
                <div class="how_simple_item">
                    <div class="_svg">
                        <svg width="72" height="72" viewBox="0 0 72 72" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M36 6V18" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M36 54V66" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M14.79 14.79L23.28 23.28" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M48.7197 48.72L57.2097 57.21" stroke="#54AB68" stroke-width="2"
                                stroke-linecap="round" stroke-linejoin="round" />
                            <path d="M6 36H18" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M54 36H66" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M14.79 57.21L23.28 48.72" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M48.7197 23.28L57.2097 14.79" stroke="#54AB68" stroke-width="2"
                                stroke-linecap="round" stroke-linejoin="round" />
                        </svg>
                    </div>
                    <div class="_body">
                        <h4>Secure</h4>
                        <p>Do you want to learn more about what Investing really means — and how to go about it?</p>
                    </div>
                </div>
                <div class="how_simple_item">
                    <div class="_svg">
                        <svg width="72" height="72" viewBox="0 0 72 72" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M36 66C52.5685 66 66 52.5685 66 36C66 19.4315 52.5685 6 36 6C19.4315 6 6 19.4315 6 36C6 52.5685 19.4315 66 36 66Z"
                                stroke="#54AB68" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            <path d="M66 36H54" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M18 36H6" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M36 18V6" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                            <path d="M36 66V54" stroke="#54AB68" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" />
                        </svg>
                    </div>
                    <div class="_body">
                        <h4>Social Impact</h4>
                        <p>Do you want to understand how Wealth Creation is possible within your circumstances?</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="safe_keep">
        <div class="section_container max_width">
            <div class="safe_keep_body">
                <h2>We keep your money safe</h2>
                <p>Your security and trust are important to us. We’re committed to protecting your account with the
                    highest standards of security available.</p>
            </div>
            <div class="_svg_con">
                <svg width="344" height="298" viewBox="0 0 344 298" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g clip-path="url(#clip0)">
                        <path d="M157.361 86.5612H165.018V19.9443H157.361V86.5612Z" fill="#48CB48" />
                        <path d="M171.81 86.5612H179.469V46.7014H171.81V86.5612Z" fill="#48CB48" />
                        <path
                            d="M159.904 132.513V193.935H129.102C125.758 193.935 123.047 191.234 123.047 187.896V138.56C123.047 135.221 125.758 132.513 129.102 132.513H159.904V132.513Z"
                            fill="#E5E5E5" />
                        <path
                            d="M197.256 138.56V187.896C197.256 191.234 194.544 193.935 191.208 193.935H159.905V132.513H191.208C194.544 132.513 197.256 135.221 197.256 138.56Z"
                            fill="#C3C4C2" />
                        <path
                            d="M160.151 146.669C156.817 146.669 154.112 149.367 154.112 152.694C154.112 154.858 155.254 156.75 156.969 157.814V162.535C156.969 166.736 163.332 166.737 163.332 162.535V157.814C165.045 156.75 166.189 154.858 166.189 152.694C166.189 149.33 163.442 146.669 160.151 146.669V146.669Z"
                            fill="#EAF5F3" />
                        <path
                            d="M159.906 101.813V108.632C149.205 108.743 140.789 116.023 140.789 132.522H133.956C133.956 110.493 147.106 101.917 159.906 101.813Z"
                            fill="#E5E5E5" />
                        <path
                            d="M186.345 132.522H179.515C179.515 115.415 170.525 108.632 160.149 108.632H159.906V101.813C159.988 101.809 160.067 101.809 160.149 101.809C173.838 101.809 186.345 111.233 186.345 132.522Z"
                            fill="#C3C4C2" />
                        <path
                            d="M160.07 218.14C124.63 218.14 95.9012 189.447 95.9012 154.055C95.9012 118.66 124.63 89.9686 160.07 89.9686C195.509 89.9686 224.239 118.66 224.239 154.055C224.239 189.447 195.509 218.14 160.07 218.14V218.14ZM160.07 81.92C120.18 81.92 87.8433 114.215 87.8433 154.055C87.8433 193.892 120.18 226.187 160.07 226.187C199.958 226.187 232.296 193.892 232.296 154.055C232.296 114.215 199.958 81.92 160.07 81.92V81.92Z"
                            fill="#48CB48" />
                        <path
                            d="M95.1975 125.255L62.0599 91.852L73.1403 71.6712L53.7447 52.2999L66.3817 32.4691L48.0952 14.2071L53.5096 8.79834L76.1402 31.4002L63.5035 51.2296L82.6177 70.3193L71.5363 90.4996L100.612 119.847L95.1975 125.255Z"
                            fill="#48CB48" />
                        <path
                            d="M93.6571 104.179L81.4941 92.0308L93.1772 71.2492L63.8726 41.9843L69.2884 36.5756L102.697 69.939L91.0124 90.7208L93.6571 93.3621L118.07 68.9831L117.613 50.3842L99.8426 32.6358L105.257 27.2271L125.196 47.1371L125.807 72.0724L93.6571 104.179Z"
                            fill="#48CB48" />
                        <path
                            d="M53.8104 2.48922C57.134 5.80842 57.134 11.1895 53.8104 14.5087C50.4885 17.8263 45.1004 17.8263 41.7768 14.5087C38.4533 11.1895 38.4533 5.80842 41.7768 2.48922C45.1004 -0.829742 50.4885 -0.829742 53.8104 2.48922V2.48922Z"
                            fill="#E6E6E6" />
                        <path
                            d="M169.698 16.5452C169.698 21.2386 165.888 25.0432 161.189 25.0432C156.488 25.0432 152.68 21.2386 152.68 16.5452C152.68 11.8517 156.488 8.04578 161.189 8.04578C165.888 8.04578 169.698 11.8517 169.698 16.5452V16.5452Z"
                            fill="#E6E6E6" />
                        <path
                            d="M184.149 39.0749C184.149 43.7686 180.339 47.5743 175.64 47.5743C170.94 47.5743 167.129 43.7686 167.129 39.0749C167.129 34.3815 170.94 30.5771 175.64 30.5771C180.339 30.5771 184.149 34.3815 184.149 39.0749V39.0749Z"
                            fill="#E6E6E6" />
                        <path d="M197.906 98.1833L192.49 92.7746L239.658 45.6685L245.073 51.0772L197.906 98.1833Z"
                            fill="#48CB48" />
                        <path d="M208.124 108.388L202.708 102.979L230.93 74.7928L236.347 80.2016L208.124 108.388Z"
                            fill="#48CB48" />
                        <path
                            d="M250.789 51.9786C247.466 55.2973 242.078 55.2973 238.754 51.9786C235.432 48.6594 235.432 43.2792 238.754 39.9602C242.078 36.6415 247.466 36.6415 250.789 39.9602C254.112 43.2792 254.112 48.6594 250.789 51.9786Z"
                            fill="#E6E6E6" />
                        <path
                            d="M245.055 78.1155C241.732 81.4333 236.343 81.4333 233.021 78.1155C229.698 74.7954 229.698 69.415 233.021 66.0965C236.343 62.7773 241.732 62.7773 245.055 66.0965C248.378 69.415 248.378 74.7954 245.055 78.1155Z"
                            fill="#E6E6E6" />
                        <path d="M140.341 85.8685H147.999V46.0077H140.341V85.8685Z" fill="#48CB48" />
                        <path
                            d="M152.68 38.4802C152.68 43.1736 148.87 46.9782 144.171 46.9782C139.471 46.9782 135.66 43.1736 135.66 38.4802C135.66 33.7868 139.471 29.9822 144.171 29.9822C148.87 29.9822 152.68 33.7868 152.68 38.4802Z"
                            fill="#E6E6E6" />
                        <path d="M155.037 286.102H162.696V219.485H155.037V286.102Z" fill="#48CB48" />
                        <path d="M140.586 244.684H148.246V220.181H140.586V244.684Z" fill="#48CB48" />
                        <path
                            d="M150.357 289.503C150.357 284.808 154.165 281.004 158.866 281.004C163.566 281.004 167.375 284.808 167.375 289.503C167.375 294.197 163.566 298 158.866 298C154.165 298 150.357 294.197 150.357 289.503Z"
                            fill="#E6E6E6" />
                        <path
                            d="M135.907 252.31C135.907 247.616 139.716 243.811 144.415 243.811C149.116 243.811 152.924 247.616 152.924 252.31C152.924 257.003 149.116 260.807 144.415 260.807C139.716 260.807 135.907 257.003 135.907 252.31Z"
                            fill="#E6E6E6" />
                        <path d="M172.057 260.04H179.716V220.181H172.057V260.04Z" fill="#48CB48" />
                        <path
                            d="M167.375 267.568C167.375 262.873 171.185 259.07 175.886 259.07C180.584 259.07 184.395 262.873 184.395 267.568C184.395 272.261 180.584 276.066 175.886 276.066C171.185 276.066 167.375 272.261 167.375 267.568V267.568Z"
                            fill="#E6E6E6" />
                        <path d="M24.541 157.878H91.2463V150.229H24.541V157.878Z" fill="#48CB48" />
                        <path d="M51.3325 143.446H91.2461V135.797H51.3325V143.446Z" fill="#48CB48" />
                        <path
                            d="M21.1366 145.554C25.8361 145.554 29.6471 149.359 29.6471 154.055C29.6471 158.747 25.8361 162.553 21.1366 162.553C16.4384 162.553 12.6274 158.747 12.6274 154.055C12.6274 149.359 16.4384 145.554 21.1366 145.554V145.554Z"
                            fill="#E6E6E6" />
                        <path
                            d="M43.6976 131.124C48.3972 131.124 52.2065 134.928 52.2065 139.622C52.2065 144.315 48.3972 148.121 43.6976 148.121C38.9969 148.121 35.1885 144.315 35.1885 139.622C35.1885 134.928 38.9969 131.124 43.6976 131.124V131.124Z"
                            fill="#E6E6E6" />
                        <path d="M16.0645 174.874H91.246V167.225H16.0645V174.874Z" fill="#48CB48" />
                        <path
                            d="M8.52718 162.553C13.2267 162.553 17.0365 166.357 17.0365 171.049C17.0365 175.744 13.2267 179.548 8.52718 179.548C3.82739 179.548 0.0180664 175.744 0.0180664 171.049C0.0180664 166.357 3.82739 162.553 8.52718 162.553Z"
                            fill="#E6E6E6" />
                        <path
                            d="M106.06 21.4186C109.383 24.7376 109.383 30.1177 106.06 33.4365C102.737 36.7554 97.3493 36.7554 94.0262 33.4365C90.7015 30.1177 90.7015 24.7376 94.0262 21.4186C97.3493 18.0994 102.737 18.0994 106.06 21.4186V21.4186Z"
                            fill="#E6E6E6" />
                        <path
                            d="M27.5533 261.177L22.1392 255.77L44.7693 233.169L64.6257 245.789L83.739 226.701L103.947 237.768L129.334 212.412L136.663 215.908L105.3 247.231L85.0932 236.165L65.6965 255.535L45.8398 242.916L27.5533 261.177Z"
                            fill="#48CB48" />
                        <path
                            d="M55.3658 245.42L49.95 240.011L83.3585 206.647L104.168 218.316L106.812 215.675L82.4013 191.293L63.7769 191.75L46.0064 209.497L40.5903 204.09L60.5267 184.18L85.4947 183.566L117.642 215.675L105.48 227.822L84.6702 216.154L55.3658 245.42Z"
                            fill="#48CB48" />
                        <path
                            d="M15.8204 255.469C19.1437 252.15 24.5324 252.15 27.8554 255.469C31.1778 258.788 31.1778 264.168 27.8554 267.487C24.5324 270.806 19.1437 270.806 15.8204 267.487C12.4974 264.168 12.4974 258.788 15.8204 255.469Z"
                            fill="#E6E6E6" />
                        <path
                            d="M34.7745 203.289C38.0981 199.969 43.4853 199.969 46.8086 203.289C50.1317 206.607 50.1317 211.988 46.8086 215.307C43.4853 218.626 38.0981 218.626 34.7745 215.307C31.4515 211.988 31.4515 206.607 34.7745 203.289Z"
                            fill="#E6E6E6" />
                        <path
                            d="M331.219 179.407H299.214L294.108 156.46H267.079L260.625 134.364H222.014V126.716H266.369L272.823 148.813H300.252L305.358 171.758H331.219V179.407Z"
                            fill="#48CB48" />
                        <path
                            d="M281.579 202.918H253.385L235.297 185.72V140.313H252.5L258.954 163.259H300.396V170.908H253.15L246.696 147.962H242.956V182.442L256.448 195.269H281.579V202.918Z"
                            fill="#48CB48" />
                        <path
                            d="M335.472 184.079C330.774 184.079 326.964 180.276 326.964 175.582C326.964 170.889 330.774 167.083 335.472 167.083C340.173 167.083 343.982 170.889 343.982 175.582C343.982 180.276 340.173 184.079 335.472 184.079V184.079Z"
                            fill="#E6E6E6" />
                        <path
                            d="M285.125 207.593C280.425 207.593 276.616 203.788 276.616 199.094C276.616 194.401 280.425 190.596 285.125 190.596C289.826 190.596 293.634 194.401 293.634 199.094C293.634 203.788 289.826 207.593 285.125 207.593Z"
                            fill="#E6E6E6" />
                        <path
                            d="M221.515 119.847L216.049 114.489L250.273 79.6639L263.907 86.6691L260.403 93.4699L251.787 89.0433L221.515 119.847Z"
                            fill="#48CB48" />
                        <path
                            d="M268.173 96.0787C264.849 99.3979 259.461 99.3979 256.137 96.0787C252.814 92.7597 252.814 87.3795 256.137 84.0603C259.461 80.7416 264.849 80.7416 268.173 84.0603C271.495 87.3795 271.495 92.7597 268.173 96.0787Z"
                            fill="#E6E6E6" />
                        <path
                            d="M233.506 248.053L226.697 244.554L231.141 235.923L202.708 207.527L208.124 202.118L240.508 234.461L233.506 248.053Z"
                            fill="#48CB48" />
                        <path
                            d="M224.086 252.312C220.762 248.993 220.762 243.612 224.086 240.293C227.408 236.974 232.795 236.974 236.118 240.293C239.442 243.612 239.442 248.993 236.118 252.312C232.795 255.631 227.408 255.631 224.086 252.312Z"
                            fill="#E6E6E6" />
                    </g>
                    <defs>
                        <clipPath id="clip0">
                            <rect width="344" height="298" fill="white" />
                        </clipPath>
                    </defs>
                </svg>
            </div>
        </div>
    </section>
    <section class="app max_width section_container">
        <div class="app_description">
            <h4>Grow your money...
                any time, any where.</h4>
            <p>Download our mobile app on the Google Playstore and App Store to sponsor different opportunities and
                monitor your sponsorships on the go.</p>
            <div class="app_container">
                <img src="storage/img/logo/playstore.svg" alt="Playstore logo">
                <img src="storage/img/logo/appstore.svg" alt="Appstore logo">
            </div>
        </div>
        <div class="mobile_app">
            <img src="storage/img/app.png" alt="Our mobile app">
        </div>
    </section>
    <section class="news_letter ">
        <div class="section_container">
            <div class="news_letter_body">
                <p>Join our mailing list to be the first to know when new opportunities are available for sponsorship.
                </p>
                <form action="">
                    <div class="form_group p_relative">
                        <label for="fristname">Firstname</label>
                        <input type="text" name="firstname">
                        <span>No spamming, it’s a promise.</span>
                    </div>
                    <div class="form_group">
                        <label for="fristname">Email</label>
                        <input type="email" name="firstname">
                    </div>
                    <button>Subscribe</button>
                </form>
            </div>
        </div>
    </section>
@endsection