<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Crowdyvest') }}</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon_io/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon_io/favicon-16x16.png">
    <link rel="manifest" href="img/favicon_io/site.webmanifest">
    <link rel="stylesheet" href="{{ asset('css/cv.min.css') }}">
    <link rel="stylesheet" href="{{ asset('fonts/stylesheet.css') }}">
    <meta name="theme-color" content="hsl(190, 50%, 92%)">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>

<body>
    <div class="dashboard_grid">
        <div id="activate">
            <div class="logo_">
                <a href="{{ url('/') }}">
                    <img src="storage/img/logo/Crowdyvest_logo.svg" alt="Crowdyvest logo">
                </a>
            </div>
            <button id="burger" class="open-main-nav">
                <span class="burger"></span>
            </button>
        </div>
        <div class="dash_overlay "></div>
        <aside class="dash_aside slider hide_class" id="add_class_on_mobile">
            <div class="aside_container">
                <div class="aside_header">
                <div class="logo_cont">
                    <a href="{{ route('dashboard') }}" class="cv_dash_logo_link">
                        <img src="{{ asset('storage/img/logo/dash_logo.svg') }}" alt="Crowdyvest logo" class="img_fluid">
                    </a>
                </div>
                    <div class="profile_img">
                        @if(Auth::user()->profile)
                            @if(file_exists(Auth::user()->profile->propix))
                                <img src="{{ asset('storage/propixs/'.Auth::user()->profile->propix) }}" alt="{{ Auth::user()->first_name }}'s profile image">
                            @else
                                <img src="{{ asset('storage/img/logo/Crowdyvest_logo.svg') }}" alt="{{ Auth::user()->first_name }}'s profile image">
                            @endif
                        @endif
                    </div>
                    <div class="users_name">
                        <p>Hello <a href="#">{{ Auth::user()->first_name }}</a></p>
                        <span>{{ Auth::user()->email }}</span>
                    </div>
                </div>
                <nav class="dash_nav">
                    <ul>
                        <li><a href="{{ route('dashboard') }}" class="active">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect x="2" y="3" width="20" height="4" fill="#54AB68"/>
                                <rect x="3" y="9" width="18" height="12" fill="#ECF5C2"/>
                                <path d="M21 8V21H3V8" stroke="#293737" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M23 3H1V8H23V3Z" stroke="#293737" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M10 12H14" stroke="#293737" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>

                            Dashboard</a></li>
                        <li><a href="{{ route('sponsorships') }}">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect x="2" y="3" width="20" height="4" fill="#54AB68"/>
                                <rect x="3" y="9" width="18" height="12" fill="#ECF5C2"/>
                                <path d="M21 8V21H3V8" stroke="#293737" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M23 3H1V8H23V3Z" stroke="#293737" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M10 12H14" stroke="#293737" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>

                            Sponsorships</a></li>
                        <li><a href="{{ route('savings') }}">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect x="2" y="3" width="20" height="4" fill="#54AB68"/>
                                <rect x="3" y="9" width="18" height="12" fill="#ECF5C2"/>
                                <path d="M21 8V21H3V8" stroke="#293737" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M23 3H1V8H23V3Z" stroke="#293737" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M10 12H14" stroke="#293737" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>

                            Savings</a></li>
                        <li><a href="#">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect x="2" y="3" width="20" height="4" fill="#54AB68"/>
                                <rect x="3" y="9" width="18" height="12" fill="#ECF5C2"/>
                                <path d="M21 8V21H3V8" stroke="#293737" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M23 3H1V8H23V3Z" stroke="#293737" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M10 12H14" stroke="#293737" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                                Wallet</a></li>
                        <li><a href="#">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect x="2" y="3" width="20" height="4" fill="#54AB68"/>
                                <rect x="3" y="9" width="18" height="12" fill="#ECF5C2"/>
                                <path d="M21 8V21H3V8" stroke="#293737" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M23 3H1V8H23V3Z" stroke="#293737" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M10 12H14" stroke="#293737" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                                Transactions</a></li>
                        <li><a href="#">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect x="2" y="3" width="20" height="4" fill="#54AB68"/>
                                <rect x="3" y="9" width="18" height="12" fill="#ECF5C2"/>
                                <path d="M21 8V21H3V8" stroke="#293737" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M23 3H1V8H23V3Z" stroke="#293737" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M10 12H14" stroke="#293737" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                                Payment</a></li>
                        <li><a href="#">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect x="2" y="3" width="20" height="4" fill="#54AB68"/>
                                <rect x="3" y="9" width="18" height="12" fill="#ECF5C2"/>
                                <path d="M21 8V21H3V8" stroke="#293737" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M23 3H1V8H23V3Z" stroke="#293737" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M10 12H14" stroke="#293737" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                                Referral</a></li>
                    </ul>
                </nav>
            </div>
        </aside>

        @yield('content')

        </div>
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="{{ asset('js/custom.js') }}"></script>
        <script src="{{ asset('js/user.js') }}"></script>
        <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script> -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>


    </body>

</html>
