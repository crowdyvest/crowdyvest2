<ul class="nav_ul">
    <li>
        <a href="#">sponsor</a>
    </li>
    <li>
        <a href="#">save</a>
    </li>
    <li><a href="#">sponsorshop</a></li>
    <li><a href="#">smartVest</a></li>
    @guest
        <li class="sign_in">
            <a href="{{ route('login') }}">sign in</a>
        </li>
        @if (Route::has('register'))
            <li class="register">
                <a href="{{ route('register') }}">register</a>
            </li>
        @endif
    @else
        <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
        <li class="register">
            <a id="navbarDropdown {{ Auth::user()->first_name }}" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                <span class="caret"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    @endguest

    <span class="line"></span>
</ul>
