@extends('layouts.app-layout')

@section('content')
<section class="dash_body sponsorship_check">
            <h3 class="dash_main_title">Savings</h3>
            <div class="dashboard_container">
                <div class="opportunities ">
            
           
                    <nav class="savings_tab">
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-SaveVault-fund-tab" data-toggle="tab" href="#nav-SaveVault-fund"
                                role="tab" aria-controls="nav-SaveVault-fund" aria-selected="true">SaveVault</a>
                            <a class="nav-item nav-link" id="nav-SaveFlex-fund-tab" data-toggle="tab" href="#nav-SaveFlex-fund" role="tab"
                                aria-controls="nav-SaveFlex-fund" aria-selected="false">SaveFlex</a>
                            <a class="nav-item nav-link" id="nav-SaveTribe-fund-tab" data-toggle="tab" href="#nav-SaveTribe-fund"
                                role="tab" aria-controls="nav-SaveTribe-fund" aria-selected="false">SaveTribe</a>
                        </div>
                    </nav>
                    <div class="tab-content savings_body" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-SaveVault-fund" role="tabpanel"
                            aria-labelledby="nav-SaveVault-fund-tab">
                            <div class="title_container">
                                <svg width="64" height="64" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="32" cy="32" r="32" fill="white" fill-opacity="0.63"/>
                                    <path d="M38.9629 18.8435L44.8495 20.1192L49.1971 17.8565L43.6278 16.7173L38.9629 18.8435Z" fill="#54BA68"/>
                                    <path d="M39.6769 52.3671L45.3287 53.5127L44.913 20.0526L38.9629 18.8118L39.6769 52.3671Z" fill="#30AB68"/>
                                    <path d="M44.8271 20.1224L45.3285 53.5127L49.8601 51.1295L49.2064 17.8534L44.8271 20.1224Z" fill="#54AB68"/>
                                    <path d="M25.7871 25.936L31.6706 27.2085L36.0181 24.9491L30.4488 23.8098L25.7871 25.936Z" fill="#54BA68"/>
                                    <path d="M26.2663 49.8664L31.9181 51.012L31.7372 27.145L25.7871 25.901L26.2663 49.8664Z" fill="#30AB68"/>
                                    <path d="M31.6514 27.2149L31.9179 51.0121L36.4527 48.6289L36.0306 24.9427L31.6514 27.2149Z" fill="#54AB68"/>
                                    <path d="M12.1826 30.7595L18.0692 32.0352L22.4168 29.7725L16.8475 28.6333L12.1826 30.7595Z" fill="#54BA68"/>
                                    <path d="M12.4746 47.3087L18.1264 48.4543L18.1359 31.9686L12.1826 30.7278L12.4746 47.3087Z" fill="#30AB68"/>
                                    <path d="M18.0508 32.0384L18.1269 48.4543L22.6585 46.0711L22.43 29.7662L18.0508 32.0384Z" fill="#54AB68"/>
                                    <path d="M9 25.7298L16.6732 29.6806L23.5309 20.9602L30.9725 25.0887L45.1544 9.33606" stroke="#BED171" stroke-width="2" stroke-linecap="square" stroke-linejoin="round"/>
                                    <path d="M41.667 8.43475L46.2874 8L46.1415 12.649" stroke="#BED171" stroke-width="2" stroke-linecap="square" stroke-linejoin="round"/>
                                    <path d="M20.7255 45.0873L14.5977 48.8001L31.3436 52.4717L38.0934 48.7684L20.7255 45.0873Z" fill="#BED171"/>
                                    <path d="M14.823 52.3575L31.553 55.88L38.1632 52.3353L38.0934 48.7684L31.3436 52.4718L14.5977 48.8002L14.823 52.3575Z" fill="#ACBE67"/>
                                    <path d="M20.9355 46.1663L17.1338 48.3845L31.0142 51.5515L35.3522 49.1873L20.9355 46.1663Z" fill="#619E3C"/>
                                    <path d="M17.9303 47.5975C17.9303 47.5975 19.0727 48.3147 18.2476 48.8383L16.7529 48.4321L17.9303 47.5975Z" fill="#BED171"/>
                                    <path d="M20.1953 46.3377C20.4957 46.516 20.8445 46.5954 21.1924 46.5646C21.5403 46.5339 21.8698 46.3945 22.1342 46.1663L21.252 45.7633L20.1953 46.3377Z" fill="#BED171"/>
                                    <path d="M29.5762 51.4754C29.9105 51.2428 30.3018 51.1055 30.7082 51.0781C31.1145 51.0507 31.5207 51.1343 31.8832 51.3199L31.1089 51.9006L29.5762 51.4754Z" fill="#BED171"/>
                                    <path d="M35.0096 49.6538C35.0096 49.6538 33.9814 49.2857 34.486 48.8002L35.9775 49.1175L35.0096 49.6538Z" fill="#BED171"/>
                                    <path d="M19.8882 53.4809L19.8184 49.9934L26.9553 46.436L32.3818 47.5594L25.0354 51.1263L25.0957 54.5504L19.8882 53.4809Z" fill="#98CB98"/>
                                </svg>
                                 <p>Set up your savings plan for a minimum of 3 months and earn decent returns on your savings.</p>   
                            </div>
                            <div class="form_grid">
                                <form action="">
                                    <div class="form_container">
                                        <label for="title">What would you like to name this savings plan?</label>
                                        <input type="text" placeholder="e.g. Ikoyi House Rent" maxlength="30">
                                    </div>
                                    <div class="form_group">
                                        <div class="form_container">
                                            <label for="title">How much would you like to save? </label>
                                           <div class="input_include">
                                               <span>NGN</span>
                                            <input type="number" placeholder="400000" >
                                           </div>
                                        </div>
                                        <div class="form_container">
                                            <label for="title">How often would you like to save? </label>
                                            <select>
                                                <option value="1">Select interval</option>
                                                <option value="2">Daily</option>
                                                <option value="3">Weekly</option>
                                                <option value="4">Monthly</option>
                                                <option value="5">Yearly</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form_group">
                                        <div class="form_container">
                                            <label for="startDate">Savings start date</label>
                                            <input type="date" name="startDate" >
                                        </div>
                                        <div class="form_container">
                                            <label for="endDate">Savings end date</label>
                                           <input type="date" name="endDate">
                                        </div>
                                    </div>
                                    <div class="form_group">
                                        <div class="form_container">
                                            <button>Create Plan</button>
                                        </div>
                                    </div>
                                </form>
                                <div>
                                    <div class="more_info">
                                        <div class="svg_contain">
                                            <svg width="61" height="61" viewBox="0 0 61 61" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <g clip-path="url(#clip0)">
                                                <path d="M30.4995 60.999C47.3439 60.999 60.999 47.3439 60.999 30.4995C60.999 13.6551 47.3439 0 30.4995 0C13.6551 0 0 13.6551 0 30.4995C0 47.3439 13.6551 60.999 30.4995 60.999Z" fill="#FF9354"/>
                                                <path d="M61.0003 30.4998C61.0003 27.4931 60.563 24.5888 59.7524 21.8446L49.4538 11.5461L40.3289 14.6404L32.6069 8.87781L32.73 18.5122L30.4494 20.125L22.1043 11.5461L20.9044 14.9058L17.5447 16.1057L28.9679 28.5733L9.875 47.6663L22.0097 59.801C24.7051 60.5807 27.5536 60.9999 30.5001 60.9999C47.345 60.9999 61.0003 47.3446 61.0003 30.4998Z" fill="#D8602C"/>
                                                <path d="M9.87555 47.6658L13.335 51.1252L36.2372 28.223L32.7778 24.7636L9.87555 47.6658Z" fill="white"/>
                                                <path d="M11.6039 49.3948L13.334 51.1249L36.2362 28.2226L34.5062 26.4926L11.6039 49.3948Z" fill="#E1E1E3"/>
                                                <path d="M49.4537 11.5463L46.3593 20.6711L52.1219 28.3932L42.4875 28.2699L36.9242 36.1367L34.0642 26.9357L24.8633 24.0757L32.7299 18.5124L32.6068 8.87805L40.3288 14.6406L49.4537 11.5463Z" fill="#FFDF47"/>
                                                <path d="M23.3034 17.3056L26.6632 16.1059L23.3034 14.906L22.1036 11.5463L20.9038 14.906L17.5439 16.1059L20.9038 17.3056L22.1036 20.6654L23.3034 17.3056Z" fill="white"/>
                                                <path d="M35.2633 42.9007L38.623 41.7009L35.2633 40.501L34.0634 37.1412L32.8637 40.501L29.5039 41.7009L32.8637 42.9007L34.0634 46.2605L35.2633 42.9007Z" fill="#E1E1E3"/>
                                                <path d="M50.1969 38.3412L53.5566 37.1413L50.1969 35.9414L48.997 32.5818L47.7973 35.9414L44.4375 37.1413L47.7973 38.3412L48.997 41.7009L50.1969 38.3412Z" fill="#E1E1E3"/>
                                                <path d="M52.1216 28.393L46.359 20.671L49.4533 11.5463L49.4529 11.5464L34.0635 26.9357H34.0638L36.9239 36.1367L42.4872 28.2698L52.1216 28.393Z" fill="#FEC000"/>
                                                </g>
                                                <defs>
                                                <clipPath id="clip0">
                                                <rect width="61" height="61" fill="white"/>
                                                </clipPath>
                                                </defs>
                                            </svg>
                                            <p>By <span>Aug 16 2020</span>, you’ll have <span>₦308,485.01</span> in your SaveVault when you save <span>₦50,000</span> monthly at 10% interest p.a.</p> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-SaveFlex-fund" role="tabpanel" aria-labelledby="nav-SaveFlex-fund-tab">
                            <div class="title_container">
                                <svg width="64" height="64" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="32" cy="32" r="32" fill="white" fill-opacity="0.63"/>
                                    <path d="M38.9629 18.8435L44.8495 20.1192L49.1971 17.8565L43.6278 16.7173L38.9629 18.8435Z" fill="#54BA68"/>
                                    <path d="M39.6769 52.3671L45.3287 53.5127L44.913 20.0526L38.9629 18.8118L39.6769 52.3671Z" fill="#30AB68"/>
                                    <path d="M44.8271 20.1224L45.3285 53.5127L49.8601 51.1295L49.2064 17.8534L44.8271 20.1224Z" fill="#54AB68"/>
                                    <path d="M25.7871 25.936L31.6706 27.2085L36.0181 24.9491L30.4488 23.8098L25.7871 25.936Z" fill="#54BA68"/>
                                    <path d="M26.2663 49.8664L31.9181 51.012L31.7372 27.145L25.7871 25.901L26.2663 49.8664Z" fill="#30AB68"/>
                                    <path d="M31.6514 27.2149L31.9179 51.0121L36.4527 48.6289L36.0306 24.9427L31.6514 27.2149Z" fill="#54AB68"/>
                                    <path d="M12.1826 30.7595L18.0692 32.0352L22.4168 29.7725L16.8475 28.6333L12.1826 30.7595Z" fill="#54BA68"/>
                                    <path d="M12.4746 47.3087L18.1264 48.4543L18.1359 31.9686L12.1826 30.7278L12.4746 47.3087Z" fill="#30AB68"/>
                                    <path d="M18.0508 32.0384L18.1269 48.4543L22.6585 46.0711L22.43 29.7662L18.0508 32.0384Z" fill="#54AB68"/>
                                    <path d="M9 25.7298L16.6732 29.6806L23.5309 20.9602L30.9725 25.0887L45.1544 9.33606" stroke="#BED171" stroke-width="2" stroke-linecap="square" stroke-linejoin="round"/>
                                    <path d="M41.667 8.43475L46.2874 8L46.1415 12.649" stroke="#BED171" stroke-width="2" stroke-linecap="square" stroke-linejoin="round"/>
                                    <path d="M20.7255 45.0873L14.5977 48.8001L31.3436 52.4717L38.0934 48.7684L20.7255 45.0873Z" fill="#BED171"/>
                                    <path d="M14.823 52.3575L31.553 55.88L38.1632 52.3353L38.0934 48.7684L31.3436 52.4718L14.5977 48.8002L14.823 52.3575Z" fill="#ACBE67"/>
                                    <path d="M20.9355 46.1663L17.1338 48.3845L31.0142 51.5515L35.3522 49.1873L20.9355 46.1663Z" fill="#619E3C"/>
                                    <path d="M17.9303 47.5975C17.9303 47.5975 19.0727 48.3147 18.2476 48.8383L16.7529 48.4321L17.9303 47.5975Z" fill="#BED171"/>
                                    <path d="M20.1953 46.3377C20.4957 46.516 20.8445 46.5954 21.1924 46.5646C21.5403 46.5339 21.8698 46.3945 22.1342 46.1663L21.252 45.7633L20.1953 46.3377Z" fill="#BED171"/>
                                    <path d="M29.5762 51.4754C29.9105 51.2428 30.3018 51.1055 30.7082 51.0781C31.1145 51.0507 31.5207 51.1343 31.8832 51.3199L31.1089 51.9006L29.5762 51.4754Z" fill="#BED171"/>
                                    <path d="M35.0096 49.6538C35.0096 49.6538 33.9814 49.2857 34.486 48.8002L35.9775 49.1175L35.0096 49.6538Z" fill="#BED171"/>
                                    <path d="M19.8882 53.4809L19.8184 49.9934L26.9553 46.436L32.3818 47.5594L25.0354 51.1263L25.0957 54.5504L19.8882 53.4809Z" fill="#98CB98"/>
                                </svg>
                                 <p>Start saving on your own terms with no restrictions, earn decent interent while at it.</p>   
                            </div>
                            <div class="form_grid">
                                <form action="">
                                    <div class="form_container">
                                        <label for="title">What would you like to name this savings plan?</label>
                                        <input type="text" placeholder="e.g. Ikoyi House Rent" maxlength="30">
                                    </div>
                                    <div class="form_group">
                                        <div class="form_container span_2">
                                            <label for="title">How much would you like to save? </label>
                                           <div class="input_include">
                                               <span>NGN</span>
                                            <input type="number" placeholder="400000" >
                                           </div>
                                        </div>
                                    </div>
                                    <div class="form_group">
                                        <div class="form_container">
                                            <button>Start Saving</button>
                                        </div>
                                    </div>
                                </form>
                               
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-SaveTribe-fund" role="tabpanel"
                            aria-labelledby="nav-SaveTribe-fund-tab">
                            <div class="opportunities_grid ">
                                <a href="#" class="add_new">
                                    <div class="svg_con">
                                        <svg width="64" height="64" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <circle cx="32" cy="32" r="32" fill="#E4FAE1"/>
                                            <path d="M31.56 27.56V17H36.456V27.56H47.016V32.456H36.456V43.016H31.56V32.456H21V27.56H31.56Z" fill="#54AB68"/>
                                            </svg>
                                            
                                    </div>
                                    <p>Create a new plan</p>
                                </a>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </section>
@endsection



