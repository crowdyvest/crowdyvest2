@extends('layouts.app-layout')

@section('content')

<section class="dash_body">
            <h3 class="dash_main_title">Cart <span>(3 items)</span></h3>
            <div class="dashboard_container">
                <div class="cart_title_grid">
                    <p>Title</p>
                    <p>Amount</p>
                </div>
                <div class="cart_items_grid">
                    <div class="item_detail">
                        <div class="svg">
                            <svg width="181" height="186" viewBox="0 0 181 186" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <circle cx="93" cy="98" r="88" fill="white" fill-opacity="0.38" />
                                <circle cx="28" cy="28" r="28" fill="white" fill-opacity="0.38" />
                                <path d="M74.46 117.135L143.909 132.421L142.443 61.9936L73.2676 53L74.46 117.135Z"
                                    fill="#2C9E60" />
                                <path d="M42 112.364L115.548 126.769L143.122 106.471L73.5821 94.3521L42 112.364Z"
                                    fill="#54BA68" />
                                <path d="M43.1924 141.173L116.152 156.176L115.548 126.769L42 112.364L43.1924 141.173Z"
                                    fill="#30AB68" />
                                <path d="M115.548 126.769L116.152 156.177L144 132.088L143.313 106.38L115.548 126.769Z"
                                    fill="#54AB68" />
                                <path d="M73.6569 53L46.5132 76.5561L118.968 89.1737L142.833 61.9936L73.6569 53Z"
                                    fill="#30AB68" />
                                <path d="M73.6569 53L46.5132 76.5561L57.4187 78.6175L84.0076 54.3715L73.6569 53Z"
                                    fill="#98CB98" />
                                <path
                                    d="M92.0644 55.4188L65.6494 79.831L77.3085 81.909L101.148 56.5908L92.0644 55.4188Z"
                                    fill="#98CB98" />
                                <path
                                    d="M111.052 57.8875L85.1748 83.5049L98.8129 85.7492L123.058 59.4501L111.052 57.8875Z"
                                    fill="#98CB98" />
                                <path
                                    d="M131.215 60.5306L106.919 87.1456L118.968 89.1737L142.832 61.9935L131.215 60.5306Z"
                                    fill="#98CB98" />
                                <path
                                    d="M61.1694 132.271L95.865 138.954L95.9395 137.125L61.2357 130.343L61.1694 132.271Z"
                                    fill="white" />
                                <path
                                    d="M68.0342 131.656L58.6937 138.381C56.4731 139.994 54.5965 142.037 53.1745 144.39C51.7524 146.742 50.8135 149.357 50.4131 152.079L50.0239 154.722L74.5675 160L74.8159 157.282C75.0743 154.44 75.9171 151.683 77.2911 149.185C78.665 146.687 80.5403 144.503 82.7984 142.769L91.4102 136.17L68.0342 131.656Z"
                                    fill="#BED171" />
                                <path
                                    d="M70.1539 132.063L61.4924 138.829C57.8738 141.456 54.5699 145.762 53.9157 150.2L53.5928 152.328L72.1164 156.426L72.3151 154.232C72.7374 149.602 76.0414 145.196 79.718 142.37L88.5864 135.654L70.1539 132.063Z"
                                    fill="#619E3C" />
                                <path
                                    d="M68.4067 156.193C68.4067 156.193 69.6902 151.588 72.9942 152.943L72.5884 157.282L68.4067 156.193Z"
                                    fill="#BED171" />
                                <path
                                    d="M52.3426 148.272C52.9185 148.347 53.4712 148.547 53.9623 148.858C54.4534 149.17 54.871 149.584 55.1863 150.074C55.5015 150.564 55.7067 151.116 55.7876 151.694C55.8685 152.271 55.8232 152.859 55.6548 153.417L51.2661 152.677L52.3426 148.272Z"
                                    fill="#BED171" />
                                <path
                                    d="M84.5703 134.856C84.5703 134.856 78.658 142.063 72.1494 140.675C65.6409 139.287 70.9819 132.795 72.2323 132.471L84.5703 134.856Z"
                                    fill="#BED171" />
                            </svg>
                        </div>
                        <div class="_ditails">
                            <p class="_name">Poultry Farm</p>
                            <p class="_duration">19% returns in 6 months</p>
                            <p class="item__price">₦1,400,000</p><!-- this element only displays from 425px down -->
                            <p class="_type">Project Sponsorshop</p>
                        </div>
                    </div>
                    <div class="item_price">
                        <!-- this p element is hidden from 425px screen size down -->
                        <p>₦1,400,000</p>
                        <div class="delete">
                            <span>
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M3 6H5H21" stroke="#EA4335" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M8 6V4C8 3.46957 8.21071 2.96086 8.58579 2.58579C8.96086 2.21071 9.46957 2 10 2H14C14.5304 2 15.0391 2.21071 15.4142 2.58579C15.7893 2.96086 16 3.46957 16 4V6M19 6V20C19 20.5304 18.7893 21.0391 18.4142 21.4142C18.0391 21.7893 17.5304 22 17 22H7C6.46957 22 5.96086 21.7893 5.58579 21.4142C5.21071 21.0391 5 20.5304 5 20V6H19Z" stroke="#EA4335" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M10 11V17" stroke="#EA4335" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M14 11V17" stroke="#EA4335" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </span>
                                                            
                        </div>
                    </div>
                </div>
                <div class="cart_items_grid pooled">
                    <div class="item_detail">
                        <div class="svg">
                            <svg width="181" height="186" viewBox="0 0 181 186" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <circle cx="93" cy="98" r="88" fill="white" fill-opacity="0.38" />
                                <circle cx="28" cy="28" r="28" fill="white" fill-opacity="0.38" />
                                <path d="M74.46 117.135L143.909 132.421L142.443 61.9936L73.2676 53L74.46 117.135Z"
                                    fill="#2C9E60" />
                                <path d="M42 112.364L115.548 126.769L143.122 106.471L73.5821 94.3521L42 112.364Z"
                                    fill="#54BA68" />
                                <path d="M43.1924 141.173L116.152 156.176L115.548 126.769L42 112.364L43.1924 141.173Z"
                                    fill="#30AB68" />
                                <path d="M115.548 126.769L116.152 156.177L144 132.088L143.313 106.38L115.548 126.769Z"
                                    fill="#54AB68" />
                                <path d="M73.6569 53L46.5132 76.5561L118.968 89.1737L142.833 61.9936L73.6569 53Z"
                                    fill="#30AB68" />
                                <path d="M73.6569 53L46.5132 76.5561L57.4187 78.6175L84.0076 54.3715L73.6569 53Z"
                                    fill="#98CB98" />
                                <path
                                    d="M92.0644 55.4188L65.6494 79.831L77.3085 81.909L101.148 56.5908L92.0644 55.4188Z"
                                    fill="#98CB98" />
                                <path
                                    d="M111.052 57.8875L85.1748 83.5049L98.8129 85.7492L123.058 59.4501L111.052 57.8875Z"
                                    fill="#98CB98" />
                                <path
                                    d="M131.215 60.5306L106.919 87.1456L118.968 89.1737L142.832 61.9935L131.215 60.5306Z"
                                    fill="#98CB98" />
                                <path
                                    d="M61.1694 132.271L95.865 138.954L95.9395 137.125L61.2357 130.343L61.1694 132.271Z"
                                    fill="white" />
                                <path
                                    d="M68.0342 131.656L58.6937 138.381C56.4731 139.994 54.5965 142.037 53.1745 144.39C51.7524 146.742 50.8135 149.357 50.4131 152.079L50.0239 154.722L74.5675 160L74.8159 157.282C75.0743 154.44 75.9171 151.683 77.2911 149.185C78.665 146.687 80.5403 144.503 82.7984 142.769L91.4102 136.17L68.0342 131.656Z"
                                    fill="#BED171" />
                                <path
                                    d="M70.1539 132.063L61.4924 138.829C57.8738 141.456 54.5699 145.762 53.9157 150.2L53.5928 152.328L72.1164 156.426L72.3151 154.232C72.7374 149.602 76.0414 145.196 79.718 142.37L88.5864 135.654L70.1539 132.063Z"
                                    fill="#619E3C" />
                                <path
                                    d="M68.4067 156.193C68.4067 156.193 69.6902 151.588 72.9942 152.943L72.5884 157.282L68.4067 156.193Z"
                                    fill="#BED171" />
                                <path
                                    d="M52.3426 148.272C52.9185 148.347 53.4712 148.547 53.9623 148.858C54.4534 149.17 54.871 149.584 55.1863 150.074C55.5015 150.564 55.7067 151.116 55.7876 151.694C55.8685 152.271 55.8232 152.859 55.6548 153.417L51.2661 152.677L52.3426 148.272Z"
                                    fill="#BED171" />
                                <path
                                    d="M84.5703 134.856C84.5703 134.856 78.658 142.063 72.1494 140.675C65.6409 139.287 70.9819 132.795 72.2323 132.471L84.5703 134.856Z"
                                    fill="#BED171" />
                            </svg>
                        </div>
                        <div class="_ditails">
                            <p class="_name">CV39402 Sponsorship</p>
                            <p class="_duration">19% returns in 6 months</p>
                            <p class="item__price">₦400,000</p><!-- this element only displays from 425px down -->
                            <p class="_type">Project Sponsorshop</p>
                        </div>
                    </div>
                    <div class="item_price">
                        <!-- this p element is hidden from 425px screen size down -->
                        <p>₦400,000</p>
                        <div class="delete">
                            <span>
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M3 6H5H21" stroke="#EA4335" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M8 6V4C8 3.46957 8.21071 2.96086 8.58579 2.58579C8.96086 2.21071 9.46957 2 10 2H14C14.5304 2 15.0391 2.21071 15.4142 2.58579C15.7893 2.96086 16 3.46957 16 4V6M19 6V20C19 20.5304 18.7893 21.0391 18.4142 21.4142C18.0391 21.7893 17.5304 22 17 22H7C6.46957 22 5.96086 21.7893 5.58579 21.4142C5.21071 21.0391 5 20.5304 5 20V6H19Z" stroke="#EA4335" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M10 11V17" stroke="#EA4335" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M14 11V17" stroke="#EA4335" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </span>
                                                            
                        </div>
                    </div>
                </div>
                <div class="cart_items_grid pooled">
                    <div class="item_detail">
                        <div class="svg">
                            <svg width="181" height="186" viewBox="0 0 181 186" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <circle cx="93" cy="98" r="88" fill="white" fill-opacity="0.38" />
                                <circle cx="28" cy="28" r="28" fill="white" fill-opacity="0.38" />
                                <path d="M74.46 117.135L143.909 132.421L142.443 61.9936L73.2676 53L74.46 117.135Z"
                                    fill="#2C9E60" />
                                <path d="M42 112.364L115.548 126.769L143.122 106.471L73.5821 94.3521L42 112.364Z"
                                    fill="#54BA68" />
                                <path d="M43.1924 141.173L116.152 156.176L115.548 126.769L42 112.364L43.1924 141.173Z"
                                    fill="#30AB68" />
                                <path d="M115.548 126.769L116.152 156.177L144 132.088L143.313 106.38L115.548 126.769Z"
                                    fill="#54AB68" />
                                <path d="M73.6569 53L46.5132 76.5561L118.968 89.1737L142.833 61.9936L73.6569 53Z"
                                    fill="#30AB68" />
                                <path d="M73.6569 53L46.5132 76.5561L57.4187 78.6175L84.0076 54.3715L73.6569 53Z"
                                    fill="#98CB98" />
                                <path
                                    d="M92.0644 55.4188L65.6494 79.831L77.3085 81.909L101.148 56.5908L92.0644 55.4188Z"
                                    fill="#98CB98" />
                                <path
                                    d="M111.052 57.8875L85.1748 83.5049L98.8129 85.7492L123.058 59.4501L111.052 57.8875Z"
                                    fill="#98CB98" />
                                <path
                                    d="M131.215 60.5306L106.919 87.1456L118.968 89.1737L142.832 61.9935L131.215 60.5306Z"
                                    fill="#98CB98" />
                                <path
                                    d="M61.1694 132.271L95.865 138.954L95.9395 137.125L61.2357 130.343L61.1694 132.271Z"
                                    fill="white" />
                                <path
                                    d="M68.0342 131.656L58.6937 138.381C56.4731 139.994 54.5965 142.037 53.1745 144.39C51.7524 146.742 50.8135 149.357 50.4131 152.079L50.0239 154.722L74.5675 160L74.8159 157.282C75.0743 154.44 75.9171 151.683 77.2911 149.185C78.665 146.687 80.5403 144.503 82.7984 142.769L91.4102 136.17L68.0342 131.656Z"
                                    fill="#BED171" />
                                <path
                                    d="M70.1539 132.063L61.4924 138.829C57.8738 141.456 54.5699 145.762 53.9157 150.2L53.5928 152.328L72.1164 156.426L72.3151 154.232C72.7374 149.602 76.0414 145.196 79.718 142.37L88.5864 135.654L70.1539 132.063Z"
                                    fill="#619E3C" />
                                <path
                                    d="M68.4067 156.193C68.4067 156.193 69.6902 151.588 72.9942 152.943L72.5884 157.282L68.4067 156.193Z"
                                    fill="#BED171" />
                                <path
                                    d="M52.3426 148.272C52.9185 148.347 53.4712 148.547 53.9623 148.858C54.4534 149.17 54.871 149.584 55.1863 150.074C55.5015 150.564 55.7067 151.116 55.7876 151.694C55.8685 152.271 55.8232 152.859 55.6548 153.417L51.2661 152.677L52.3426 148.272Z"
                                    fill="#BED171" />
                                <path
                                    d="M84.5703 134.856C84.5703 134.856 78.658 142.063 72.1494 140.675C65.6409 139.287 70.9819 132.795 72.2323 132.471L84.5703 134.856Z"
                                    fill="#BED171" />
                            </svg>
                        </div>
                        <div class="_ditails">
                            <p class="_name">CV39402 Sponsorship</p>
                            <p class="_duration">19% returns in 6 months</p>
                            <p class="item__price">₦400,000</p><!-- this element only displays from 425px down -->
                            <p class="_type">Project Sponsorshop</p>
                        </div>
                    </div>
                    <div class="item_price">
                        <!-- this p element is hidden from 425px screen size down -->
                        <p>₦400,000</p>
                        <div class="delete">
                            <span>
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M3 6H5H21" stroke="#EA4335" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M8 6V4C8 3.46957 8.21071 2.96086 8.58579 2.58579C8.96086 2.21071 9.46957 2 10 2H14C14.5304 2 15.0391 2.21071 15.4142 2.58579C15.7893 2.96086 16 3.46957 16 4V6M19 6V20C19 20.5304 18.7893 21.0391 18.4142 21.4142C18.0391 21.7893 17.5304 22 17 22H7C6.46957 22 5.96086 21.7893 5.58579 21.4142C5.21071 21.0391 5 20.5304 5 20V6H19Z" stroke="#EA4335" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M10 11V17" stroke="#EA4335" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M14 11V17" stroke="#EA4335" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </span>
                                                            
                        </div>
                    </div>
                </div>
                <div class="_line_"></div>
                <div class="total_">
                    <div class="total_figure">
                        <p class="_subject">Total</p>
                        <p class="_figure">₦1,800,000</p>
                    </div>

                </div>
                <div class="_line_"></div>
                <div class="total_">
                    <div class="submit">
                        <button>Checkout</button>
                    </div>

                </div>
            </div>
        </section>
@endsection



