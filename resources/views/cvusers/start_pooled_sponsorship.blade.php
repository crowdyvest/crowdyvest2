@extends('layouts.app-layout')

@section('content')
<section class="dash_body sponsorship_check">
            <h3 class="dash_main_title">Pooled Sponsorshop</h3>
            <div class="dashboard_container">
                <div class="opportunities ">

                    <div class="t savings_body" id="">
                        <div class="title_container">
                            <svg width="64" height="64" viewBox="0 0 64 64" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <circle cx="32" cy="32" r="32" fill="white" fill-opacity="0.63" />
                                <path
                                    d="M38.9629 18.8435L44.8495 20.1192L49.1971 17.8565L43.6278 16.7173L38.9629 18.8435Z"
                                    fill="#54BA68" />
                                <path
                                    d="M39.6769 52.3671L45.3287 53.5127L44.913 20.0526L38.9629 18.8118L39.6769 52.3671Z"
                                    fill="#30AB68" />
                                <path
                                    d="M44.8271 20.1224L45.3285 53.5127L49.8601 51.1295L49.2064 17.8534L44.8271 20.1224Z"
                                    fill="#54AB68" />
                                <path
                                    d="M25.7871 25.936L31.6706 27.2085L36.0181 24.9491L30.4488 23.8098L25.7871 25.936Z"
                                    fill="#54BA68" />
                                <path d="M26.2663 49.8664L31.9181 51.012L31.7372 27.145L25.7871 25.901L26.2663 49.8664Z"
                                    fill="#30AB68" />
                                <path
                                    d="M31.6514 27.2149L31.9179 51.0121L36.4527 48.6289L36.0306 24.9427L31.6514 27.2149Z"
                                    fill="#54AB68" />
                                <path
                                    d="M12.1826 30.7595L18.0692 32.0352L22.4168 29.7725L16.8475 28.6333L12.1826 30.7595Z"
                                    fill="#54BA68" />
                                <path
                                    d="M12.4746 47.3087L18.1264 48.4543L18.1359 31.9686L12.1826 30.7278L12.4746 47.3087Z"
                                    fill="#30AB68" />
                                <path
                                    d="M18.0508 32.0384L18.1269 48.4543L22.6585 46.0711L22.43 29.7662L18.0508 32.0384Z"
                                    fill="#54AB68" />
                                <path d="M9 25.7298L16.6732 29.6806L23.5309 20.9602L30.9725 25.0887L45.1544 9.33606"
                                    stroke="#BED171" stroke-width="2" stroke-linecap="square" stroke-linejoin="round" />
                                <path d="M41.667 8.43475L46.2874 8L46.1415 12.649" stroke="#BED171" stroke-width="2"
                                    stroke-linecap="square" stroke-linejoin="round" />
                                <path
                                    d="M20.7255 45.0873L14.5977 48.8001L31.3436 52.4717L38.0934 48.7684L20.7255 45.0873Z"
                                    fill="#BED171" />
                                <path
                                    d="M14.823 52.3575L31.553 55.88L38.1632 52.3353L38.0934 48.7684L31.3436 52.4718L14.5977 48.8002L14.823 52.3575Z"
                                    fill="#ACBE67" />
                                <path
                                    d="M20.9355 46.1663L17.1338 48.3845L31.0142 51.5515L35.3522 49.1873L20.9355 46.1663Z"
                                    fill="#619E3C" />
                                <path
                                    d="M17.9303 47.5975C17.9303 47.5975 19.0727 48.3147 18.2476 48.8383L16.7529 48.4321L17.9303 47.5975Z"
                                    fill="#BED171" />
                                <path
                                    d="M20.1953 46.3377C20.4957 46.516 20.8445 46.5954 21.1924 46.5646C21.5403 46.5339 21.8698 46.3945 22.1342 46.1663L21.252 45.7633L20.1953 46.3377Z"
                                    fill="#BED171" />
                                <path
                                    d="M29.5762 51.4754C29.9105 51.2428 30.3018 51.1055 30.7082 51.0781C31.1145 51.0507 31.5207 51.1343 31.8832 51.3199L31.1089 51.9006L29.5762 51.4754Z"
                                    fill="#BED171" />
                                <path
                                    d="M35.0096 49.6538C35.0096 49.6538 33.9814 49.2857 34.486 48.8002L35.9775 49.1175L35.0096 49.6538Z"
                                    fill="#BED171" />
                                <path
                                    d="M19.8882 53.4809L19.8184 49.9934L26.9553 46.436L32.3818 47.5594L25.0354 51.1263L25.0957 54.5504L19.8882 53.4809Z"
                                    fill="#98CB98" />
                            </svg>
                            <p>Earn returns on highly vetted opportunities in our pooled sponsorship vehicle.</p>
                        </div>
                        <div class="form_grid">
                            <form action="">
                                <div class="form_container">
                                    <label for="title">What would you like to name this sponsorship?</label>
                                    <input type="text" placeholder="e.g. Ikoyi House Rent" maxlength="30">
                                </div>

                                <div class="pooled_range">
                                    <div class="input_flex">
                                        <p>Amount</p>
                                        <div class="div">
                                            <span>₦</span>
                                            <input type="number">
                                        </div>
                                    </div>
                                    <div class="slider_container">
                                        <input type="range" value="50">
                                    </div>
                                    <div class="span_flex">
                                        <p>₦30,000</p>
                                        <p>₦2,000,000</p>
                                    </div>
                                </div>
                                <div class="pooled_range">
                                    <div class="input_flex">
                                        <p>Tenor</p>
                                        <div class="input_border">
                                            <input type="number" value="3">
                                            <span>months</span>
                                        </div>
                                    </div>
                                    <div class="slider_container">
                                        <input type="range" value="50">
                                    </div>
                                    <div class="span_flex">
                                        <p>3 months</p>
                                        <p>24 months</p>
                                    </div>
                                </div>
                                <div class="form_group">
                                    <div class="form_container">
                                        <button>Create Plan</button>
                                    </div>
                                </div>
                            </form>
                            <div>
                                <div class="more_info pooled_sponsorshop">
                                    <div class="svg_contain">
                                        <svg width="61" height="61" viewBox="0 0 61 61" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <g clip-path="url(#clip0)">
                                                <path
                                                    d="M30.4995 60.999C47.3439 60.999 60.999 47.3439 60.999 30.4995C60.999 13.6551 47.3439 0 30.4995 0C13.6551 0 0 13.6551 0 30.4995C0 47.3439 13.6551 60.999 30.4995 60.999Z"
                                                    fill="#54AB68" />
                                                <path
                                                    d="M61.0003 30.4999C61.0003 27.4932 60.563 24.5889 59.7524 21.8447L49.4538 11.5462L40.3289 14.6405L32.6069 8.87793L32.73 18.5124L30.4494 20.1252L22.1043 11.5462L20.9044 14.906L17.5447 16.1058L28.9679 28.5734L9.875 47.6664L22.0097 59.8011C24.7051 60.5808 27.5536 61 30.5001 61C47.345 61 61.0003 47.3447 61.0003 30.4999Z"
                                                    fill="#2D8A43" />
                                                <path
                                                    d="M9.87653 47.6658L13.3359 51.1252L36.2382 28.223L32.7788 24.7636L9.87653 47.6658Z"
                                                    fill="white" />
                                                <path
                                                    d="M11.6039 49.395L13.334 51.125L36.2362 28.2228L34.5062 26.4927L11.6039 49.395Z"
                                                    fill="#E1E1E3" />
                                                <path
                                                    d="M49.4537 11.5462L46.3593 20.6709L52.1219 28.393L42.4875 28.2697L36.9242 36.1366L34.0642 26.9356L24.8633 24.0756L32.7299 18.5122L32.6068 8.87793L40.3288 14.6405L49.4537 11.5462Z"
                                                    fill="#FFDF47" />
                                                <path
                                                    d="M23.3044 17.3055L26.6642 16.1058L23.3044 14.9059L22.1046 11.5461L20.9048 14.9059L17.5449 16.1058L20.9048 17.3055L22.1046 20.6653L23.3044 17.3055Z"
                                                    fill="white" />
                                                <path
                                                    d="M35.2633 42.9009L38.623 41.701L35.2633 40.5011L34.0634 37.1414L32.8637 40.5011L29.5039 41.701L32.8637 42.9009L34.0634 46.2606L35.2633 42.9009Z"
                                                    fill="#E1E1E3" />
                                                <path
                                                    d="M50.1969 38.3412L53.5566 37.1413L50.1969 35.9414L48.997 32.5818L47.7973 35.9414L44.4375 37.1413L47.7973 38.3412L48.997 41.7009L50.1969 38.3412Z"
                                                    fill="#E1E1E3" />
                                                <path
                                                    d="M52.1226 28.3929L46.36 20.6709L49.4543 11.5461L49.4538 11.5463L34.0645 26.9355H34.0648L36.9249 36.1365L42.4882 28.2697L52.1226 28.3929Z"
                                                    fill="#FEC000" />
                                            </g>
                                            <defs>
                                                <clipPath id="clip0">
                                                    <rect width="61" height="61" fill="white" />
                                                </clipPath>
                                            </defs>
                                        </svg>
                                    </div>
                                    <div class="output">
                                        <div class="p_container">
                                            <p><span>12%</span> per annum</p>
                                        </div>
                                        <div class="p_container">
                                            <p>Returns earned at maturity</p>
                                            <span>₦7,000</span>
                                        </div>
                                        <div class="p_container">
                                            <p>Total Recievable at maturity</p>
                                            <span>₦57,000</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_line_"></div>
                        <div class="project_details">
                            <h4>Socially responsible & viable</h4>

                            <div class="sdg_section">
                                <div class="sdg_item">
                                    <p>Nunc purus, potenti molestie purus est turpis sed etiam pharetra. Adipiscing
                                        convallis interdum mauris at imperdiet diam. Cras dictumst eget pharetra,
                                        pretium lorem elit. Fermentum viverra ultricies vel etiam habitant. Ut pretium,
                                        amet lacus sit vitae feugiat amet volutpat neque. Felis ac imperdiet amet sed.
                                    </p>

                                    <p> Nunc purus, potenti molestie purus est turpis sed etiam pharetra. Adipiscing
                                        convallis interdum mauris at imperdiet diam. Cras dictumst eget pharetra,
                                        pretium lorem elit. Fermentum viverra ultricies vel etiam habitant. Ut pretium,
                                        amet lacus sit vitae feugiat amet volutpat neque. Felis ac imperdiet amet sed.
                                    </p>

                                </div>
                                <div class="sdg_svg">
                                    <img src="img/sdg.jpg" alt="SDG image">
                                </div>
                            </div>
                        </div>
                        <div class="project_partners">
                            <h4>Sponsorships Partners</h4>
                            <p>Nunc purus, potenti molestie purus est turpis sed etiam pharetra. Adipiscing convallis interdum mauris at imperdiet diam.</p>
                            <div class="logos">
                                <div class="img_container">
                                    <img src="img/logo/fc.png" alt="Partners Logo">
                                </div>
                                <div class="img_container">
                                    <img src="img/logo/pw.png" alt="Partners Logo">
                                </div>
                                <div class="img_container">
                                    <img src="img/logo/lead.png" alt="Partners Logo">
                                </div>
                                <div class="img_container">
                                    <img src="img/logo/capital.png" alt="Partners Logo">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

@endsection



