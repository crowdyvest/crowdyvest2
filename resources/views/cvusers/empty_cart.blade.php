@extends('layouts.app-layout')

@section('content')
<section class="dash_body">
            <h3 class="dash_main_title">Cart <span>(0 items)</span></h3>
            <div class="dashboard_container">
                <div class="empty_cart">
                    <svg width="205" height="161" viewBox="0 0 205 161" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="124.5" cy="80.5" r="80.5" fill="#FFFCED" fill-opacity="0.73"/>
                        <circle cx="53.5" cy="65.5" r="53.5" fill="#C1F2BB" fill-opacity="0.22"/>
                        <circle cx="87" cy="124" r="28" fill="#DAF4F1" fill-opacity="0.55"/>
                        <path d="M92.7506 117.167C94.6376 117.167 96.1673 115.637 96.1673 113.75C96.1673 111.863 94.6376 110.333 92.7506 110.333C90.8637 110.333 89.334 111.863 89.334 113.75C89.334 115.637 90.8637 117.167 92.7506 117.167Z" stroke="#DD8833" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M130.333 117.167C132.22 117.167 133.749 115.637 133.749 113.75C133.749 111.863 132.22 110.333 130.333 110.333C128.446 110.333 126.916 111.863 126.916 113.75C126.916 115.637 128.446 117.167 130.333 117.167Z" stroke="#DD8833" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M65.416 45.4167H79.0827L88.2393 91.1659C88.5518 92.7389 89.4075 94.1519 90.6568 95.1576C91.9061 96.1633 93.4692 96.6975 95.0727 96.6667H128.283C129.886 96.6975 131.449 96.1633 132.699 95.1576C133.948 94.1519 134.804 92.7389 135.116 91.1659L140.583 62.5001H82.4993" stroke="#DD8833" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </div>
                <div class="empty_cart_body">
                    <p class="_title">Your cart is empty</p>
                    <p>Don’t let it stay empty. Let’s add items from the Sponsorshop.</p>
                    <a href="new_sponsorship.html">Add items from Sponsorshop</a>
                </div>
            </div>
        </section>

@endsection



