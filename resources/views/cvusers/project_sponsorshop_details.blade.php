@extends('layouts.app-layout')

@section('content')
<section class="dash_body sponsorship_check">
            <h3 class="dash_main_title">Projects Sponsorshop</h3>
            <div class="dashboard_container">
                <div class="opportunities ">
                    <div class="project_img">
                        <div class="img__container">
                            <img src="img/ginger.jpg" alt="Project image">
                        </div>
                        <div class="product_price">
                            <h4>Beef Processing Project</h4>
                            <p class="company_">a Farmcrowdy Project</p>
                            <div class="percentage">
                                <div class="roi">
                                    <p>12%</p>
                                    <span>per annum</span>
                                </div>
                                <div class="roi">
                                    <p>6</p>
                                    <span>months</span>

                                </div>
                            </div>
                            <div class="more_details">
                                <a href="#p_details">More on project details </a>
                            </div>
                        </div>
                    </div>
                    <div class="t savings_body main_product_details" id="">

                        <div class="form_grid">
                            <form action="">
                                <div class="form_container">
                                    <label for="title">What would you like to name this sponsorship?</label>
                                    <input type="text" placeholder="e.g. Ikoyi House Rent" maxlength="30">
                                </div>

                                <div class="pooled_range">
                                    <div class="input_flex">
                                        <p>Amount</p>
                                        <div class="div">
                                            <span>₦</span>
                                            <input type="number">
                                        </div>
                                    </div>
                                    <div class="slider_container">
                                        <input type="range" value="50">
                                    </div>
                                    <div class="span_flex">
                                        <p>₦30,000</p>
                                        <p>₦2,000,000</p>
                                    </div>
                                </div>
                                <div class="form_group">
                                    <div class="form_container">
                                        <button>Create Plan</button>
                                    </div>
                                </div>
                            </form>
                            <div>
                                <div class="more_info pooled_sponsorshop">

                                    <div class="output">
                                        <div class="p_container">
                                            <p>Returns earned at maturity</p>
                                            <span>₦7,000</span>
                                        </div>
                                        <div class="p_container">
                                            <p>Total Recievable at maturity</p>
                                            <span>₦57,000</span>
                                        </div>
                                    </div>
                                    <div class="svg_contain">
                                        <svg width="61" height="61" viewBox="0 0 61 61" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <g clip-path="url(#clip0)">
                                                <path
                                                    d="M30.4995 60.999C47.3439 60.999 60.999 47.3439 60.999 30.4995C60.999 13.6551 47.3439 0 30.4995 0C13.6551 0 0 13.6551 0 30.4995C0 47.3439 13.6551 60.999 30.4995 60.999Z"
                                                    fill="#54AB68" />
                                                <path
                                                    d="M61.0003 30.4999C61.0003 27.4932 60.563 24.5889 59.7524 21.8447L49.4538 11.5462L40.3289 14.6405L32.6069 8.87793L32.73 18.5124L30.4494 20.1252L22.1043 11.5462L20.9044 14.906L17.5447 16.1058L28.9679 28.5734L9.875 47.6664L22.0097 59.8011C24.7051 60.5808 27.5536 61 30.5001 61C47.345 61 61.0003 47.3447 61.0003 30.4999Z"
                                                    fill="#2D8A43" />
                                                <path
                                                    d="M9.87653 47.6658L13.3359 51.1252L36.2382 28.223L32.7788 24.7636L9.87653 47.6658Z"
                                                    fill="white" />
                                                <path
                                                    d="M11.6039 49.395L13.334 51.125L36.2362 28.2228L34.5062 26.4927L11.6039 49.395Z"
                                                    fill="#E1E1E3" />
                                                <path
                                                    d="M49.4537 11.5462L46.3593 20.6709L52.1219 28.393L42.4875 28.2697L36.9242 36.1366L34.0642 26.9356L24.8633 24.0756L32.7299 18.5122L32.6068 8.87793L40.3288 14.6405L49.4537 11.5462Z"
                                                    fill="#FFDF47" />
                                                <path
                                                    d="M23.3044 17.3055L26.6642 16.1058L23.3044 14.9059L22.1046 11.5461L20.9048 14.9059L17.5449 16.1058L20.9048 17.3055L22.1046 20.6653L23.3044 17.3055Z"
                                                    fill="white" />
                                                <path
                                                    d="M35.2633 42.9009L38.623 41.701L35.2633 40.5011L34.0634 37.1414L32.8637 40.5011L29.5039 41.701L32.8637 42.9009L34.0634 46.2606L35.2633 42.9009Z"
                                                    fill="#E1E1E3" />
                                                <path
                                                    d="M50.1969 38.3412L53.5566 37.1413L50.1969 35.9414L48.997 32.5818L47.7973 35.9414L44.4375 37.1413L47.7973 38.3412L48.997 41.7009L50.1969 38.3412Z"
                                                    fill="#E1E1E3" />
                                                <path
                                                    d="M52.1226 28.3929L46.36 20.6709L49.4543 11.5461L49.4538 11.5463L34.0645 26.9355H34.0648L36.9249 36.1365L42.4882 28.2697L52.1226 28.3929Z"
                                                    fill="#FEC000" />
                                            </g>
                                            <defs>
                                                <clipPath id="clip0">
                                                    <rect width="61" height="61" fill="white" />
                                                </clipPath>
                                            </defs>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_line_"></div>
                        <div class="project_details " id="p_details">
                            <h4 class="left_align">Project Details</h4>

                            <div class="sdg_section">
                                <div class="sdg_item">
                                    <p>Nunc purus, potenti molestie purus est turpis sed etiam pharetra. Adipiscing
                                        convallis interdum mauris at imperdiet diam. Cras dictumst eget pharetra,
                                        pretium lorem elit. Fermentum viverra ultricies vel etiam habitant. Ut pretium,
                                        amet lacus sit vitae feugiat amet volutpat neque. Felis ac imperdiet amet sed.
                                    </p>

                                    <p> Nunc purus, potenti molestie purus est turpis sed etiam pharetra. Adipiscing
                                        convallis interdum mauris at imperdiet diam. Cras dictumst eget pharetra,
                                        pretium lorem elit. Fermentum viverra ultricies vel etiam habitant. Ut pretium,
                                        amet lacus sit vitae feugiat amet volutpat neque. Felis ac imperdiet amet sed.
                                    </p>

                                </div>
                                <!-- <div class="sdg_svg">
                                    <img src="img/sdg.jpg" alt="SDG image">
                                </div> -->
                            </div>
                        </div>
                        <div class="project_partners pad_t ">
                            <h4>Sponsorships Partners</h4>
                            <p>Nunc purus, potenti molestie purus est turpis sed etiam pharetra. Adipiscing convallis
                                interdum mauris at imperdiet diam.</p>
                            <div class="logos">
                                <div class="img_container">
                                    <img src="img/logo/fc.png" alt="Partners Logo">
                                </div>
                                <div class="img_container">
                                    <img src="img/logo/pw.png" alt="Partners Logo">
                                </div>
                                <div class="img_container">
                                    <img src="img/logo/lead.png" alt="Partners Logo">
                                </div>
                                <div class="img_container">
                                    <img src="img/logo/capital.png" alt="Partners Logo">
                                </div>
                            </div>
                        </div>
                        <div class="project_partners pad_t">
                            <h4>Projected Impact</h4>
                            <p>Nunc purus, potenti molestie purus est turpis sed etiam pharetra. Adipiscing convallis
                                interdum mauris at imperdiet diam.</p>
                        </div>
                    </div>
                </div>

            </div>
        </section>

@endsection



