@extends('layouts.app-layout')

@section('content')
    <section class="dash_body">
        <div class="dashboard_container">
            <div class="empty_cart payment_">
                <svg width="282" height="222" viewBox="0 0 282 222" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="171.264" cy="110.737" r="110.737" fill="#FFFCED" fill-opacity="0.73"/>
                    <circle cx="73.5951" cy="90.1024" r="73.5951" fill="#C1F2BB" fill-opacity="0.22"/>
                    <circle cx="119.677" cy="170.576" r="38.5171" fill="#DAF4F1" fill-opacity="0.55"/>
                </svg>
                    
            </div>
            <div class="empty_cart_body payment_">
                <p class="_title">Payment Successful</p>
                <p>Your payment of <span>₦245,000</span> was successfully received.</p>
                <a href="dashboard.html">Go to Dashboard</a>
            </div>
        </div>
    </section>

@endsection



