@extends('layouts.app-layout')

@section('content')
<section class="dash_body sponsorship_check">
            <h3 class="dash_main_title">Savings</h3>
            <div class="dashboard_container">
                <div class="dash_stat_grid savings_stat">
                   
                   <a href="#" class="sponsorship_link">
                       <svg width="64" height="64" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="32" cy="32" r="32" fill="white" fill-opacity="0.63"/>
                        <path d="M38.9631 18.8435L44.8498 20.1192L49.1973 17.8565L43.628 16.7173L38.9631 18.8435Z" fill="#54BA68"/>
                        <path d="M39.6771 52.3671L45.3289 53.5126L44.9132 20.0525L38.9631 18.8117L39.6771 52.3671Z" fill="#30AB68"/>
                        <path d="M44.8274 20.1223L45.3288 53.5126L49.8604 51.1294L49.2067 17.8533L44.8274 20.1223Z" fill="#54AB68"/>
                        <path d="M25.7871 25.936L31.6706 27.2085L36.0181 24.9491L30.4488 23.8098L25.7871 25.936Z" fill="#54BA68"/>
                        <path d="M26.2663 49.8665L31.9181 51.012L31.7372 27.145L25.7871 25.9011L26.2663 49.8665Z" fill="#30AB68"/>
                        <path d="M31.6516 27.2148L31.9182 51.012L36.4529 48.6288L36.0309 24.9427L31.6516 27.2148Z" fill="#54AB68"/>
                        <path d="M12.1829 30.7595L18.0695 32.0352L22.417 29.7726L16.8477 28.6334L12.1829 30.7595Z" fill="#54BA68"/>
                        <path d="M12.4748 47.3087L18.1266 48.4543L18.1361 31.9686L12.1829 30.7278L12.4748 47.3087Z" fill="#30AB68"/>
                        <path d="M18.0505 32.0384L18.1267 48.4543L22.6583 46.0711L22.4298 29.7662L18.0505 32.0384Z" fill="#54AB68"/>
                        <path d="M9 25.7297L16.6732 29.6806L23.5309 20.9601L30.9725 25.0887L45.1544 9.336" stroke="#BED171" stroke-width="2" stroke-linecap="square" stroke-linejoin="round"/>
                        <path d="M41.6667 8.43475L46.2872 8L46.1412 12.649" stroke="#BED171" stroke-width="2" stroke-linecap="square" stroke-linejoin="round"/>
                        <path d="M20.7257 45.0873L14.5979 48.8002L31.3438 52.4718L38.0936 48.7685L20.7257 45.0873Z" fill="#BED171"/>
                        <path d="M14.8232 52.3575L31.5533 55.88L38.1634 52.3353L38.0936 48.7684L31.3438 52.4718L14.5979 48.8002L14.8232 52.3575Z" fill="#ACBE67"/>
                        <path d="M20.935 46.1663L17.1333 48.3845L31.0137 51.5515L35.3517 49.1873L20.935 46.1663Z" fill="#619E3C"/>
                        <path d="M17.93 47.5975C17.93 47.5975 19.0724 48.3147 18.2473 48.8383L16.7527 48.4321L17.93 47.5975Z" fill="#BED171"/>
                        <path d="M20.1956 46.3376C20.4959 46.5159 20.8447 46.5953 21.1927 46.5646C21.5406 46.5338 21.8701 46.3945 22.1345 46.1663L21.2523 45.7632L20.1956 46.3376Z" fill="#BED171"/>
                        <path d="M29.5762 51.4753C29.9105 51.2427 30.3018 51.1054 30.7082 51.078C31.1145 51.0506 31.5207 51.1342 31.8832 51.3198L31.1089 51.9006L29.5762 51.4753Z" fill="#BED171"/>
                        <path d="M35.0091 49.6538C35.0091 49.6538 33.9809 49.2857 34.4855 48.8002L35.977 49.1175L35.0091 49.6538Z" fill="#BED171"/>
                        <path d="M19.8879 53.4809L19.8181 49.9934L26.9551 46.436L32.3815 47.5594L25.0352 51.1263L25.0954 54.5504L19.8879 53.4809Z" fill="#98CB98"/>
                        </svg>
                       <p>SaveVault</p>
                       <span class="save_span">Set up your savings plan for a minimum of 3 months and earn decent returns on your savings.</span>
                        <div class="save_link">
                            <span>
                                Create Plan
                                <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M6.45117 0.486328L11.9648 6L6.45117 11.5137L5.92383 10.9863L10.5352 6.375H0V5.625H10.5352L5.92383 1.01367L6.45117 0.486328Z" fill="#CD9D09"/>
                                </svg>
                            </span> 
                        </div>
                   </a>
                    <a href="#" class="sponsorshop_link">
                        <svg width="64" height="64" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="32" cy="32" r="32" fill="white" fill-opacity="0.63"/>
                            <path d="M23.6841 36.9727L52.9616 43.4013L52.3438 13.7823L23.1814 10L23.6841 36.9727Z" fill="#2C9E60"/>
                            <path d="M10 34.9662L41.0055 41.0242L52.63 32.4878L23.314 27.3911L10 34.9662Z" fill="#54BA68"/>
                            <path d="M10.5027 47.0823L41.2603 53.392L41.0055 41.0243L10 34.9662L10.5027 47.0823Z" fill="#30AB68"/>
                            <path d="M41.0054 41.0242L41.2602 53.3919L52.9999 43.2614L52.7101 32.4493L41.0054 41.0242Z" fill="#54AB68"/>
                            <path d="M23.3455 10L11.9026 19.9068L42.4473 25.2132L52.5079 13.7823L23.3455 10Z" fill="#30AB68"/>
                            <path d="M23.3455 10L11.9026 19.9068L16.5 20.7737L27.709 10.5768L23.3455 10Z" fill="#98CB98"/>
                            <path d="M31.1055 11.0173L19.9697 21.2841L24.8848 22.158L34.9349 11.5102L31.1055 11.0173Z" fill="#98CB98"/>
                            <path d="M39.11 12.0555L28.2012 22.8292L33.9506 23.773L44.1717 12.7127L39.11 12.0555Z" fill="#98CB98"/>
                            <path d="M47.61 13.1671L37.3679 24.3603L42.4471 25.2133L52.5076 13.7824L47.61 13.1671Z" fill="#98CB98"/>
                            <path d="M18.0813 43.3383L32.7079 46.1489L32.7393 45.3798L18.1092 42.5273L18.0813 43.3383Z" fill="white"/>
                            <path d="M20.9751 43.0797L17.0375 45.9077C16.1013 46.5863 15.3102 47.4456 14.7107 48.4349C14.1112 49.4243 13.7154 50.5238 13.5466 51.6686L13.3826 52.7802L23.7294 55L23.8341 53.8569C23.943 52.6618 24.2983 51.5024 24.8775 50.4518C25.4568 49.4013 26.2473 48.4824 27.1993 47.7534L30.8297 44.9779L20.9751 43.0797Z" fill="#BED171"/>
                            <path d="M21.8689 43.251L18.2175 46.0965C16.692 47.2011 15.2991 49.0119 15.0233 50.8786L14.8872 51.7735L22.6962 53.4968L22.78 52.574C22.958 50.6269 24.3508 48.7742 25.9008 47.5856L29.6394 44.7611L21.8689 43.251Z" fill="#619E3C"/>
                            <path d="M21.1323 53.399C21.1323 53.399 21.6734 51.4624 23.0662 52.0322L22.8952 53.8569L21.1323 53.399Z" fill="#BED171"/>
                            <path d="M14.3601 50.0676C14.6028 50.0992 14.8359 50.1833 15.0429 50.3142C15.2499 50.4451 15.426 50.6196 15.5589 50.8255C15.6917 51.0314 15.7782 51.2639 15.8124 51.5067C15.8465 51.7494 15.8274 51.9968 15.7564 52.2314L13.9062 51.9203L14.3601 50.0676Z" fill="#BED171"/>
                            <path d="M27.9464 44.4255C27.9464 44.4255 25.454 47.4563 22.7102 46.8725C19.9664 46.2887 22.218 43.5586 22.7451 43.4222L27.9464 44.4255Z" fill="#BED171"/>
                            </svg>
                        <p>SaveFlex</p>
                        <span class="save_span">Start saving on your own terms with no restrictions, earn decent interest while at it.</span>
                           <div class="save_link">
                            <span>
                                Start Saving
                                  <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                      <path d="M6.45117 0.486328L11.9648 6L6.45117 11.5137L5.92383 10.9863L10.5352 6.375H0V5.625H10.5352L5.92383 1.01367L6.45117 0.486328Z" fill="#CD9D09"/>
                                  </svg>
                              </span> 
                           </div> 
                    </a>
                    <a href="#" class="sponsorshop_link">
                        <svg width="64" height="64" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="32" cy="32" r="32" fill="white" fill-opacity="0.63"/>
                            <path d="M23.6841 36.9727L52.9616 43.4013L52.3438 13.7823L23.1814 10L23.6841 36.9727Z" fill="#2C9E60"/>
                            <path d="M10 34.9662L41.0055 41.0242L52.63 32.4878L23.314 27.3911L10 34.9662Z" fill="#54BA68"/>
                            <path d="M10.5027 47.0823L41.2603 53.392L41.0055 41.0243L10 34.9662L10.5027 47.0823Z" fill="#30AB68"/>
                            <path d="M41.0054 41.0242L41.2602 53.3919L52.9999 43.2614L52.7101 32.4493L41.0054 41.0242Z" fill="#54AB68"/>
                            <path d="M23.3455 10L11.9026 19.9068L42.4473 25.2132L52.5079 13.7823L23.3455 10Z" fill="#30AB68"/>
                            <path d="M23.3455 10L11.9026 19.9068L16.5 20.7737L27.709 10.5768L23.3455 10Z" fill="#98CB98"/>
                            <path d="M31.1055 11.0173L19.9697 21.2841L24.8848 22.158L34.9349 11.5102L31.1055 11.0173Z" fill="#98CB98"/>
                            <path d="M39.11 12.0555L28.2012 22.8292L33.9506 23.773L44.1717 12.7127L39.11 12.0555Z" fill="#98CB98"/>
                            <path d="M47.61 13.1671L37.3679 24.3603L42.4471 25.2133L52.5076 13.7824L47.61 13.1671Z" fill="#98CB98"/>
                            <path d="M18.0813 43.3383L32.7079 46.1489L32.7393 45.3798L18.1092 42.5273L18.0813 43.3383Z" fill="white"/>
                            <path d="M20.9751 43.0797L17.0375 45.9077C16.1013 46.5863 15.3102 47.4456 14.7107 48.4349C14.1112 49.4243 13.7154 50.5238 13.5466 51.6686L13.3826 52.7802L23.7294 55L23.8341 53.8569C23.943 52.6618 24.2983 51.5024 24.8775 50.4518C25.4568 49.4013 26.2473 48.4824 27.1993 47.7534L30.8297 44.9779L20.9751 43.0797Z" fill="#BED171"/>
                            <path d="M21.8689 43.251L18.2175 46.0965C16.692 47.2011 15.2991 49.0119 15.0233 50.8786L14.8872 51.7735L22.6962 53.4968L22.78 52.574C22.958 50.6269 24.3508 48.7742 25.9008 47.5856L29.6394 44.7611L21.8689 43.251Z" fill="#619E3C"/>
                            <path d="M21.1323 53.399C21.1323 53.399 21.6734 51.4624 23.0662 52.0322L22.8952 53.8569L21.1323 53.399Z" fill="#BED171"/>
                            <path d="M14.3601 50.0676C14.6028 50.0992 14.8359 50.1833 15.0429 50.3142C15.2499 50.4451 15.426 50.6196 15.5589 50.8255C15.6917 51.0314 15.7782 51.2639 15.8124 51.5067C15.8465 51.7494 15.8274 51.9968 15.7564 52.2314L13.9062 51.9203L14.3601 50.0676Z" fill="#BED171"/>
                            <path d="M27.9464 44.4255C27.9464 44.4255 25.454 47.4563 22.7102 46.8725C19.9664 46.2887 22.218 43.5586 22.7451 43.4222L27.9464 44.4255Z" fill="#BED171"/>
                            </svg>
                        <p>SaveTribe</p>
                        <span class="save_span">Save funds with a Tribe for a period and earn good returns on your savings.</span>
                        <div class="save_link">
                            <span>
                                Join a Tribe
                                  <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                      <path d="M6.45117 0.486328L11.9648 6L6.45117 11.5137L5.92383 10.9863L10.5352 6.375H0V5.625H10.5352L5.92383 1.01367L6.45117 0.486328Z" fill="#CD9D09"/>
                                  </svg>
                            </span> 
                        </div> 
                    </a>
                </div>
                <div class="line_"></div>
                <div class="opportunities ">
                    <div class="opportunities_header">
                        <h4>Savings plan</h4>
                    </div>
                    <div class="sort savings_num">
                        <p>You have <span>5 Saving plans</span> running.</p>
        
                    </div>
                    <nav class="savings_tab">
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-SaveVault-tab" data-toggle="tab" href="#nav-SaveVault"
                                role="tab" aria-controls="nav-SaveVault" aria-selected="true">SaveVault</a>
                            <a class="nav-item nav-link" id="nav-SaveFlex-tab" data-toggle="tab" href="#nav-SaveFlex" role="tab"
                                aria-controls="nav-SaveFlex" aria-selected="false">SaveFlex</a>
                            <a class="nav-item nav-link" id="nav-SaveTribe-tab" data-toggle="tab" href="#nav-SaveTribe"
                                role="tab" aria-controls="nav-SaveTribe" aria-selected="false">SaveTribe</a>
                        </div>
                    </nav>
                    <div class="tab-content savings_body" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-SaveVault" role="tabpanel"
                            aria-labelledby="nav-SaveVault-tab">
                            <div class="opportunities_grid ">
                                <div class="opportunities_item">
                                    <div class="invest_title toggle_name">
                                        <div class="input_ ">
                                            <p class="title_name">CVS309940</p>
                                            <span class="type_">SaveVault</span>
                                        </div>
                                        <span class=" status_">
                                            <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="4" cy="4" r="4" fill="#30AB68"/>
                                            </svg>
                                                
                                            Active</span>
                                        <!-- <span class=" status_">
                                            <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="4" cy="4" r="4" fill="#CD9D09"/>
                                            </svg> 
                                            Completed</span> -->
                                    </div>
                                    <div class="product_body">
                                        <div class="circular_progress savings_balance">
                                              <div class="progress_amt">
                                                  <span>Balance</span>
                                                  <p>₦3,000,000</p>
                                              </div>
                                        </div>
                                        <div class="more_details">
                                            <div class="savings_roi">
                                               <p> <span>28%</span> returns</p>
                                                <p><span>6 </span>months</p>
                                            </div>
                                            <div class="_line_"></div>
                                            <div class="savings_duration">
                                                <div class="start_">
                                                    <span>Savings Date</span>
                                                    <p>12th Jan. 2020</p>
                                                </div>
                                                <div class="end_">
                                                    <span>Maturity Date</span>
                                                    <p>11th Jul. 2020</p>
                                                </div>
                                            </div>
                                            <!-- <div class="view_detail">
                                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M12 11.2969C11.6127 11.2969 11.2969 11.612 11.2969 12C11.2969 12.388 11.6127 12.7031 12 12.7031C12.3873 12.7031 12.7031 12.388 12.7031 12C12.7031 11.612 12.3873 11.2969 12 11.2969Z" fill="#888888"/>
                                                    <path d="M12 8.48438C10.0609 8.48438 8.48438 10.0616 8.48438 12C8.48438 13.9384 10.0609 15.5156 12 15.5156C13.9391 15.5156 15.5156 13.9384 15.5156 12C15.5156 10.0616 13.9391 8.48438 12 8.48438ZM12 14.1094C10.8369 14.1094 9.89062 13.1631 9.89062 12C9.89062 10.8369 10.8369 9.89062 12 9.89062C13.1631 9.89062 14.1094 10.8369 14.1094 12C14.1094 13.1631 13.1631 14.1094 12 14.1094Z" fill="#888888"/>
                                                    <path d="M23.8213 11.5908C23.6071 11.2914 18.4997 4.26562 12 4.26562C5.50031 4.26562 0.392944 11.2914 0.178711 11.5908C0.00292969 11.8352 0.00292969 12.1648 0.178711 12.4092C0.392944 12.7086 5.50031 19.7344 12 19.7344C18.4997 19.7344 23.6071 12.7086 23.8213 12.4092C23.9971 12.1648 23.9971 11.8352 23.8213 11.5908ZM12 16.9219C9.28638 16.9219 7.07812 14.7136 7.07812 12C7.07812 9.28638 9.28638 7.07812 12 7.07812C14.7136 7.07812 16.9219 9.28638 16.9219 12C16.9219 14.7136 14.7136 16.9219 12 16.9219Z" fill="#736F6F"/>
                                                    <path d="M23.2969 19.7812C22.9083 19.7812 22.5938 20.0958 22.5938 20.4844V21.5995L20.9815 19.9872C20.7068 19.7126 20.2619 19.7126 19.9872 19.9872C19.7126 20.2619 19.7126 20.7068 19.9872 20.9815L21.5995 22.5938H20.4844C20.0958 22.5938 19.7812 22.9083 19.7812 23.2969C19.7812 23.6854 20.0958 24 20.4844 24H23.2969C23.6856 24 24 23.6805 24 23.2969V20.4844C24 20.0958 23.6854 19.7812 23.2969 19.7812Z" fill="#888888"/>
                                                    <path d="M23.946 0.434692C23.8746 0.26239 23.7374 0.125244 23.5653 0.0540161C23.4794 0.0184936 23.3884 0 23.2969 0H20.4844C20.0958 0 19.7812 0.314575 19.7812 0.703125C19.7812 1.09167 20.0958 1.40625 20.4844 1.40625H21.5995L19.9872 3.01849C19.7126 3.29315 19.7126 3.7381 19.9872 4.01276C20.2619 4.28741 20.7068 4.28741 20.9815 4.01276L22.5938 2.40051V3.51562C22.5938 3.90417 22.9083 4.21875 23.2969 4.21875C23.6854 4.21875 24 3.90417 24 3.51562V0.703125C24 0.611572 23.9815 0.520569 23.946 0.434692Z" fill="#888888"/>
                                                    <path d="M4.01276 19.9872C3.7381 19.7126 3.29315 19.7126 3.01849 19.9872L1.40625 21.5995V20.4844C1.40625 20.0958 1.09167 19.7812 0.703125 19.7812C0.314575 19.7812 0 20.0958 0 20.4844V23.2969C0 23.6856 0.319519 24 0.703125 24H3.51562C3.90417 24 4.21875 23.6854 4.21875 23.2969C4.21875 22.9083 3.90417 22.5938 3.51562 22.5938H2.40051L4.01276 20.9815C4.28741 20.7068 4.28741 20.2619 4.01276 19.9872Z" fill="#888888"/>
                                                    <path d="M0.703125 4.21875C1.09167 4.21875 1.40625 3.90417 1.40625 3.51562V2.40051L3.01849 4.01276C3.29315 4.28741 3.7381 4.28741 4.01276 4.01276C4.28741 3.7381 4.28741 3.29315 4.01276 3.01849L2.40051 1.40625H3.51562C3.90417 1.40625 4.21875 1.09167 4.21875 0.703125C4.21875 0.314575 3.90417 0 3.51562 0H0.703125C0.314392 0 0 0.319519 0 0.703125V3.51562C0 3.90417 0.314575 4.21875 0.703125 4.21875Z" fill="#888888"/>
                                                </svg>
                                                <span>View Details</span>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="opportunities_item">
                                    <div class="invest_title toggle_name">
                                        <div class="input_ ">
                                            <p class="title_name">Lekki House Rent</p>
                                            <span class="type_">SaveVault</span>
                                        </div>
                                        <span class=" status_">
                                            <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="4" cy="4" r="4" fill="#CD9D09"/>
                                            </svg> 
                                            Completed</span>
                                    </div>
                                    <div class="product_body">
                                        <div class="circular_progress savings_balance">
                                              <div class="progress_amt">
                                                  <span>Balance</span>
                                                  <p>₦50,000,000</p>
                                              </div>
                                        </div>
                                        <div class="more_details">
                                            <div class="savings_roi">
                                               <p> <span>28%</span> returns</p>
                                                <p><span>6 </span>months</p>
                                            </div>
                                            <div class="_line_"></div>
                                            <div class="savings_duration">
                                                <div class="start_">
                                                    <span>Savings Date</span>
                                                    <p>12th Jan. 2020</p>
                                                </div>
                                                <div class="end_">
                                                    <span>Maturity Date</span>
                                                    <p>11th Jul. 2020</p>
                                                </div>
                                            </div>
                              
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                        </div>
                        <div class="tab-pane fade" id="nav-SaveFlex" role="tabpanel" aria-labelledby="nav-SaveFlex-tab">
                            <div class="opportunities_grid ">
                                <div class="opportunities_item">
                                    <div class="invest_title ">
                                        <div class="input_ ">
                                            <p class="title_name">Lekki House Rent</p>
                                            <span class="type_">SaveFlex</span>
                                        </div>
                                        <span class=" status_">
                                            <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="4" cy="4" r="4" fill="#30AB68"/>
                                            </svg>
                                                
                                            Active</span>
                                    </div>
                                    <div class="product_body">
                                        <div class="circular_progress savings_balance">
                                              <div class="progress_amt">
                                                  <span>Balance</span>
                                                  <p>₦50,000,000</p>
                                              </div>
                                        </div>
                                        <div class="more_details saveFlex">
                                            <div class="savings_duration">
                                                <div class="start_">
                                                    <span>Start Date</span>
                                                    <p>12th Jan. 2020</p>
                                                </div>
                                                <div class="end_">
                                                    <p class="mb-0">12%</p>
                                                    <span>per anum</span>
                                                </div>
                                            </div>
                              
                                        </div>
                                    </div>
                                </div>
                                <div class="opportunities_item">
                                    <div class="invest_title toggle_name">
                                        <div class="input_ ">
                                            <p class="title_name">Lekki House Rent</p>
                                            <span class="type_">SaveFlex</span>
                                        </div>
                                        <span class=" status_">
                                            <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="4" cy="4" r="4" fill="#30AB68"/>
                                            </svg>
                                                
                                            Active</span>
                                    </div>
                                    <div class="product_body">
                                        <div class="circular_progress savings_balance">
                                              <div class="progress_amt">
                                                  <span>Balance</span>
                                                  <p>₦50,000,000</p>
                                              </div>
                                        </div>
                                        <div class="more_details saveFlex">
                                            <!-- <div class="savings_roi">
                                               <p> <span>28%</span> returns</p>
                                                <p><span>6 </span>months</p>
                                            </div>
                                            <div class="_line_"></div> -->
                                            <div class="savings_duration">
                                                <div class="start_">
                                                    <span>Start Date</span>
                                                    <p>12th Jan. 2020</p>
                                                </div>
                                                <div class="end_">
                                                    <p class="mb-0">11%</p>
                                                    <span>per anum</span>
                                                </div>
                                            </div>
                              
                                        </div>
                                    </div>
                                </div>
                              
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-SaveTribe" role="tabpanel"
                            aria-labelledby="nav-SaveTribe-tab">
                            <div class="opportunities_grid ">
                                <a href="#" class="add_new">
                                    <div class="svg_con">
                                        <svg width="64" height="64" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <circle cx="32" cy="32" r="32" fill="#E4FAE1"/>
                                            <path d="M31.56 27.56V17H36.456V27.56H47.016V32.456H36.456V43.016H31.56V32.456H21V27.56H31.56Z" fill="#54AB68"/>
                                            </svg>
                                            
                                    </div>
                                    <p>Create a new plan</p>
                                </a>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </section>
@endsection
