@extends('layouts.app-layout')

@section('content')
<section class="dash_body">
            <h3 class="dash_main_title see_all">Sponsorship Opportunities</h3>
            <div class="sponsor_filter">
                <ul>
                    <li class="active"><a href="#">All Sectors</a></li>
                    <li><a href="#">Agriculture</a></li>
                    <li><a href="#">Transportation</a></li>
                </ul>
            </div>
            <div class="dashboard_container">
                <div class="opportunities">
                    <div class="opportunities_grid">
                        <a href="#" class="opportunities_item">
                            <div class="img_container">
                                <img src="img/car.jpg" class="project_img" alt="Sponsorship opportunity">
                            </div>
                            <div class="product_body">
                                <h5 class="product_name">Ajah-CMS Bus Route</h5>
                                <img src="img/logo/pw.svg" alt="Owner's Logo">
                                <div class="returns">
                                    <span>28% returns</span>
                                    <span>
                                        <svg width="4" height="4" viewBox="0 0 4 4" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <circle cx="2" cy="2" r="2" fill="#293737"/>
                                        </svg>
                                    </span>
                                    <span>6 months</span>
                                </div>
                                <div class="price">
                                    <span>Starting from ₦50,000</span>
                                </div>
                                <div class="sponsor_btn">
                                    <span>Sponsor Now</span>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="opportunities_item">
                            <div class="img_container">
                                <img src="img/car.jpg" class="project_img" alt="Sponsorship opportunity">
                            </div>
                            <div class="product_body">
                                <h5 class="product_name">Ajah-CMS Bus Route</h5>
                                <img src="img/logo/pw.svg" alt="Owner's Logo">
                                <div class="returns">
                                    <span>28% returns</span>
                                    <span>
                                        <svg width="4" height="4" viewBox="0 0 4 4" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <circle cx="2" cy="2" r="2" fill="#293737"/>
                                        </svg>
                                    </span>
                                    <span>6 months</span>
                                </div>
                                <div class="price">
                                    <span>Starting from ₦50,000</span>
                                </div>
                                <div class="sponsor_btn">
                                    <span>Sponsor Now</span>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="opportunities_item">
                            <div class="img_container">
                                <img src="img/ginger.jpg" class="project_img" alt="Sponsorship opportunity">
                            </div>
                            <div class="product_body">
                                <h5 class="product_name">Kaduna Ginger Farm</h5>
                                <img src="img/logo/fc.svg" alt="Owner's Logo">
                                <div class="returns">
                                    <span>18% returns</span>
                                    <span>
                                        <svg width="4" height="4" viewBox="0 0 4 4" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <circle cx="2" cy="2" r="2" fill="#293737"/>
                                        </svg>
                                    </span>
                                    <span>12 months</span>
                                </div>
                                <div class="price">
                                    <span>Starting from ₦60,000</span>
                                </div>
                                <div class="sponsor_btn">
                                    <span>Sponsor Now</span>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="opportunities_item">
                            <div class="img_container">
                                <img src="img/cattle.jpg" class="project_img" alt="Sponsorship opportunity">
                            </div>
                            <div class="product_body">
                                <h5 class="product_name">Kaduna Cattle Farm</h5>
                                <img src="img/logo/fc.svg" alt="Owner's Logo">
                                <div class="returns">
                                    <span>28% returns</span>
                                    <span>
                                        <svg width="4" height="4" viewBox="0 0 4 4" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <circle cx="2" cy="2" r="2" fill="#293737"/>
                                        </svg>
                                    </span>
                                    <span>6 months</span>
                                </div>
                                <div class="price">
                                    <span>Starting from ₦20,000</span>
                                </div>
                                <div class="sponsor_btn">
                                    <span>Sponsor Now</span>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="opportunities_item">
                            <div class="img_container">
                                <img src="img/ginger.jpg" class="project_img" alt="Sponsorship opportunity">
                            </div>
                            <div class="product_body">
                                <h5 class="product_name">Kaduna Ginger Farm</h5>
                                <img src="img/logo/fc.svg" alt="Owner's Logo">
                                <div class="returns">
                                    <span>18% returns</span>
                                    <span>
                                        <svg width="4" height="4" viewBox="0 0 4 4" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <circle cx="2" cy="2" r="2" fill="#293737"/>
                                        </svg>
                                    </span>
                                    <span>12 months</span>
                                </div>
                                <div class="price">
                                    <span>Starting from ₦60,000</span>
                                </div>
                                <div class="sponsor_btn">
                                    <span>Sponsor Now</span>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="opportunities_item">
                            <div class="img_container">
                                <img src="img/car.jpg" class="project_img" alt="Sponsorship opportunity">
                            </div>
                            <div class="product_body">
                                <h5 class="product_name">Ajah-CMS Bus Route</h5>
                                <img src="img/logo/pw.svg" alt="Owner's Logo">
                                <div class="returns">
                                    <span>28% returns</span>
                                    <span>
                                        <svg width="4" height="4" viewBox="0 0 4 4" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <circle cx="2" cy="2" r="2" fill="#293737"/>
                                        </svg>
                                    </span>
                                    <span>6 months</span>
                                </div>
                                <div class="price">
                                    <span>Starting from ₦50,000</span>
                                </div>
                                <div class="sponsor_btn">
                                    <span>Sponsor Now</span>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="opportunities_item">
                            <div class="img_container">
                                <img src="img/cattle.jpg" class="project_img" alt="Sponsorship opportunity">
                            </div>
                            <div class="product_body">
                                <h5 class="product_name">Kaduna Cattle Farm</h5>
                                <img src="img/logo/fc.svg" alt="Owner's Logo">
                                <div class="returns">
                                    <span>28% returns</span>
                                    <span>
                                        <svg width="4" height="4" viewBox="0 0 4 4" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <circle cx="2" cy="2" r="2" fill="#293737"/>
                                        </svg>
                                    </span>
                                    <span>6 months</span>
                                </div>
                                <div class="price">
                                    <span>Starting from ₦20,000</span>
                                </div>
                                <div class="sponsor_btn">
                                    <span>Sponsor Now</span>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="opportunities_item">
                            <div class="img_container">
                                <img src="img/ginger.jpg" class="project_img" alt="Sponsorship opportunity">
                            </div>
                            <div class="product_body">
                                <h5 class="product_name">Kaduna Ginger Farm</h5>
                                <img src="img/logo/fc.svg" alt="Owner's Logo">
                                <div class="returns">
                                    <span>18% returns</span>
                                    <span>
                                        <svg width="4" height="4" viewBox="0 0 4 4" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <circle cx="2" cy="2" r="2" fill="#293737"/>
                                        </svg>
                                    </span>
                                    <span>12 months</span>
                                </div>
                                <div class="price">
                                    <span>Starting from ₦60,000</span>
                                </div>
                                <div class="sponsor_btn">
                                    <span>Sponsor Now</span>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="opportunities_item">
                            <div class="img_container">
                                <img src="img/cattle.jpg" class="project_img" alt="Sponsorship opportunity">
                            </div>
                            <div class="product_body">
                                <h5 class="product_name">Kaduna Cattle Farm</h5>
                                <img src="img/logo/fc.svg" alt="Owner's Logo">
                                <div class="returns">
                                    <span>28% returns</span>
                                    <span>
                                        <svg width="4" height="4" viewBox="0 0 4 4" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <circle cx="2" cy="2" r="2" fill="#293737"/>
                                        </svg>
                                    </span>
                                    <span>6 months</span>
                                </div>
                                <div class="price">
                                    <span>Starting from ₦20,000</span>
                                </div>
                                <div class="sponsor_btn">
                                    <span>Sponsor Now</span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>

@endsection