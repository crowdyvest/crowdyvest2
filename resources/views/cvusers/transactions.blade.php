@extends('layouts.app-layout')

@section('content')
<section class="dash_body sponsorship_check">
            <h3 class="dash_main_title">Transactions</h3>
            <div class="transaction_grid">
                <div class="dashboard_container">
                    <div class="dash_stat_grid transactions">
                       
                       <a href="#" class="sponsorshop_link">
                            <svg class="_svg" width="64" height="64" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="32" cy="32" r="32" fill="white" fill-opacity="0.63"/>
                                <circle cx="32" cy="32" r="18" fill="#E4FAE1"/>
                                <g clip-path="url(#clip0)">
                                <path d="M42 32C42 26.4772 37.5228 22 32 22C26.4772 22 22 26.4772 22 32C22 37.5228 26.4772 42 32 42C37.5228 42 42 37.5228 42 32Z" stroke="#30AB68" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M36 32L32 28L28 32" stroke="#30AB68" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M32 36V28" stroke="#30AB68" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </g>
                                <defs>
                                <clipPath id="clip0">
                                <rect x="21.2002" y="18.5" width="24.3" height="24.3" fill="white"/>
                                </clipPath>
                                </defs>
                            </svg>                        
                            <div class="save_link">
                                <p>Top-up your Wallet</p>
                                <span>
                                    Add Fund
                                    <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M6.45117 0.486328L11.9648 6L6.45117 11.5137L5.92383 10.9863L10.5352 6.375H0V5.625H10.5352L5.92383 1.01367L6.45117 0.486328Z" fill="#CD9D09"/>
                                    </svg>
                                </span> 
                            </div>
                       </a>
                        <a href="#" class="sponsorshop_link">
                            <svg class="_svg" width="64" height="64" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="32" cy="32" r="32" fill="white" fill-opacity="0.63"/>
                                <circle r="18" transform="matrix(-1 0 0 1 32 32)" fill="#FAF6E4"/>
                                <g clip-path="url(#clip0)">
                                <path d="M42 32C42 37.5228 37.5228 42 32 42C26.4772 42 22 37.5228 22 32C22 26.4772 26.4772 22 32 22C37.5228 22 42 26.4772 42 32Z" stroke="#D8602C" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M36 32L32 36L28 32" stroke="#D8602C" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M32 28L32 36" stroke="#D8602C" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </g>
                                <defs>
                                <clipPath id="clip0">
                                <rect width="24.3" height="24.3" transform="matrix(-1 0 0 1 42.7998 18.5)" fill="white"/>
                                </clipPath>
                                </defs>
                            </svg>
                               <div class="save_link">
                                   <p>Withdraw from Wallet</p>
                                <span>
                                    Send Fund
                                      <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M6.45117 0.486328L11.9648 6L6.45117 11.5137L5.92383 10.9863L10.5352 6.375H0V5.625H10.5352L5.92383 1.01367L6.45117 0.486328Z" fill="#CD9D09"/>
                                      </svg>
                                  </span> 
                               </div> 
                        </a>
                        <a href="#" class="sponsorshop_link">
                            <svg class="_svg" width="64" height="64" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="32" cy="32" r="32" fill="white" fill-opacity="0.63"/>
                                <circle r="18" transform="matrix(-1 0 0 1 32 32)" fill="#FAFAFA"/>
                                <g clip-path="url(#clip0)">
                                <path d="M29.333 36.4583L32.4997 39.6249L35.6663 36.4583" stroke="#293737" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M32.5 32.5V39.625" stroke="#293737" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M39.5296 37.3212C40.2179 36.8372 40.734 36.1465 41.0031 35.3493C41.2722 34.5521 41.2803 33.6898 41.0262 32.8877C40.772 32.0856 40.2689 31.3854 39.5898 30.8886C38.9107 30.3918 38.091 30.1243 37.2496 30.1249H36.2521C36.014 29.197 35.5685 28.3352 34.9491 27.6043C34.3297 26.8735 33.5526 26.2927 32.6763 25.9056C31.8 25.5186 30.8473 25.3354 29.8899 25.3698C28.9325 25.4042 27.9954 25.6554 27.1492 26.1044C26.3029 26.5534 25.5695 27.1885 25.0043 27.962C24.439 28.7354 24.0565 29.627 23.8857 30.5697C23.7149 31.5123 23.7601 32.4814 24.018 33.404C24.2759 34.3267 24.7397 35.1788 25.3746 35.8962" stroke="#293737" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </g>
                                <defs>
                                <clipPath id="clip0">
                                <rect x="23" y="23" width="19" height="19" fill="white"/>
                                </clipPath>
                                </defs>
                                </svg>
                                
                               <div class="save_link">
                                   <p>Transfer Fund</p>
                                <span>
                                    Download Statement
                                    <svg width="9" height="12" viewBox="0 0 9 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M1.25 12V11.25H8V12H1.25ZM8.26367 6.26367L4.625 9.93164L0.986328 6.26367L1.51367 5.73633L4.25 8.49023V0H5V8.49023L7.73633 5.73633L8.26367 6.26367Z" fill="#293737"/>
                                        </svg>
                                        
                                  </span> 
                               </div> 
                        </a>
                        
                    </div>
                    <div class="line_"></div>
                    <div class="opportunities ">
                        <div class="opportunities_header">
                            <h4> History</h4>
                        </div>
                       
                       <div class="transaction_container">
                            <div class="transaction_table">
                                <form class="">
                                    <select id='mySelect' >
                                        <option value='0'> All</option>
                                        <option value='1'>Wallet Top-ups</option>
                                        <option value='2'>Payouts</option>
                                        <option value='3'>Withdrawals</option>
                                    </select>
                                </form>
                                <nav class="savings_tab" id="savings_tab">
                                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                        <a class="nav-item nav-link active" id="nav-AllSponsorships-tab" data-toggle="tab" href="#nav-AllSponsorships"
                                            role="tab" aria-controls="nav-AllSponsorships" aria-selected="true">All</a>
                                        <a class="nav-item nav-link" id="nav-Top-ups-tab" data-toggle="tab" href="#nav-Top-ups" role="tab"
                                            aria-controls="nav-Top-ups" aria-selected="false">
                                            <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="4" cy="4" r="4" fill="#54AB68"/>
                                            </svg>                                    
                                            Wallet Top-ups</a>
                                        <a class="nav-item nav-link" id="nav-Payouts-tab" data-toggle="tab" href="#nav-Payouts"
                                            role="tab" aria-controls="nav-Payouts" aria-selected="false">
                                            <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="4" cy="4" r="4" fill="#0077B5"/>
                                            </svg>
                                            Payouts</a>
            
                                        <a class="nav-item nav-link" id="nav-Withdrawals-tab" data-toggle="tab" href="#nav-Withdrawals"
                                        role="tab" aria-controls="nav-Withdrawals" aria-selected="false">
                                            <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="4" cy="4" r="4" fill="#D8602C"/>
                                            </svg>
                                            Withdrawals</a>
                                    </div>
                                </nav>
                                <div class="tab-content savings_body" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="nav-AllSponsorships" role="tabpanel"
                                        aria-labelledby="nav-AllSponsorships-tab">
                                        <div class="transaction_status_grid wallet">
                                            <div class="activity">
                                                <div class="svg_container">
                                                    <img src="img/icon/wallet.svg" alt="Wallet icon">
                                                        
                                                </div>
                                                <div class="transaction_title">
                                                    <p>Wallet funding</p>
                                                    <span>Card</span>
                                                </div>
                                            </div>
                                            <div class="amount">
                                                <p>+ ₦30,000.00</p>
                                                <span>23 December 2019</span>
                                            </div>
                                        </div>
                                        <div class="transaction_status_grid payout">
                                            <div class="activity">
                                                <div class="svg_container">
                                                    <img src="img/icon/payout.svg" alt="Wallet icon">
                                                        
                                                </div>
                                                <div class="transaction_title">
                                                    <p>Maize Project Payout</p>
                                                    <span>Bank Transfer</span>
                                                </div>
                                            </div>
                                            <div class="amount">
                                                <p>+ ₦30,000.00</p>
                                                <span>23 December 2019</span>
                                            </div>
                                        </div>
                                        <div class="transaction_status_grid Withdrawals">
                                            <div class="activity">
                                                <div class="svg_container">
                                                    <img src="img/icon/withdrawals.svg" alt="Withdrawals icon">
                                                        
                                                </div>
                                                <div class="transaction_title">
                                                    <p>Sponsorship Payout</p>
                                                    <span>Bank Transfer</span>
                                                </div>
                                            </div>
                                            <div class="amount">
                                                <p>- ₦30,000.00</p>
                                                <span>23 December 2019</span>
                                            </div>
                                        </div>
                                        <div class="transaction_status_grid wallet">
                                            <div class="activity">
                                                <div class="svg_container">
                                                    <img src="img/icon/wallet.svg" alt="Wallet icon">
                                                        
                                                </div>
                                                <div class="transaction_title">
                                                    <p>Wallet funding</p>
                                                    <span>Card</span>
                                                </div>
                                            </div>
                                            <div class="amount">
                                                <p>+ ₦50,000.00</p>
                                                <span>23 September 2019</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="nav-Top-ups" role="tabpanel" aria-labelledby="nav-Top-ups-tab">
                                        <div class="transaction_status_grid wallet">
                                            <div class="activity">
                                                <div class="svg_container">
                                                    <img src="img/icon/wallet.svg" alt="Wallet icon">
                                                        
                                                </div>
                                                <div class="transaction_title">
                                                    <p>Wallet funding</p>
                                                    <span>Card</span>
                                                </div>
                                            </div>
                                            <div class="amount">
                                                <p>+ ₦30,000.00</p>
                                                <span>23 December 2019</span>
                                            </div>
                                        </div>
                                        <div class="transaction_status_grid wallet">
                                            <div class="activity">
                                                <div class="svg_container">
                                                    <img src="img/icon/wallet.svg" alt="Wallet icon">
                                                        
                                                </div>
                                                <div class="transaction_title">
                                                    <p>Wallet funding</p>
                                                    <span>Card</span>
                                                </div>
                                            </div>
                                            <div class="amount">
                                                <p>+ ₦3,000.00</p>
                                                <span>23 December 2019</span>
                                            </div>
                                        </div>
                                        <div class="transaction_status_grid wallet">
                                            <div class="activity">
                                                <div class="svg_container">
                                                    <img src="img/icon/wallet.svg" alt="Wallet icon">
                                                        
                                                </div>
                                                <div class="transaction_title">
                                                    <p>Wallet funding</p>
                                                    <span>Card</span>
                                                </div>
                                            </div>
                                            <div class="amount">
                                                <p>+ ₦50,000.00</p>
                                                <span>23 December 2019</span>
                                            </div>
                                        </div>
                                        <div class="transaction_status_grid wallet">
                                            <div class="activity">
                                                <div class="svg_container">
                                                    <img src="img/icon/wallet.svg" alt="Wallet icon">
                                                        
                                                </div>
                                                <div class="transaction_title">
                                                    <p>Wallet funding</p>
                                                    <span>Card</span>
                                                </div>
                                            </div>
                                            <div class="amount">
                                                <p>+ ₦10,000.00</p>
                                                <span>23 December 2019</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="nav-Payouts" role="tabpanel"
                                        aria-labelledby="nav-Payouts-tab">
                                        <div class="transaction_status_grid payout">
                                            <div class="activity">
                                                <div class="svg_container">
                                                    <img src="img/icon/payout.svg" alt="Wallet icon">
                                                        
                                                </div>
                                                <div class="transaction_title">
                                                    <p>Maize Project Payout</p>
                                                    <span>Bank Transfer</span>
                                                </div>
                                            </div>
                                            <div class="amount">
                                                <p>+ ₦30,000.00</p>
                                                <span>23 December 2019</span>
                                            </div>
                                        </div>
                                        <div class="transaction_status_grid payout">
                                            <div class="activity">
                                                <div class="svg_container">
                                                    <img src="img/icon/payout.svg" alt="Wallet icon">
                                                        
                                                </div>
                                                <div class="transaction_title">
                                                    <p>Maize Project Payout</p>
                                                    <span>Bank Transfer</span>
                                                </div>
                                            </div>
                                            <div class="amount">
                                                <p>+ ₦30,000.00</p>
                                                <span>23 December 2019</span>
                                            </div>
                                        </div>
                                        <div class="transaction_status_grid payout">
                                            <div class="activity">
                                                <div class="svg_container">
                                                    <img src="img/icon/payout.svg" alt="Wallet icon">
                                                        
                                                </div>
                                                <div class="transaction_title">
                                                    <p>Maize Project Payout</p>
                                                    <span>Bank Transfer</span>
                                                </div>
                                            </div>
                                            <div class="amount">
                                                <p>+ ₦30,000.00</p>
                                                <span>23 December 2019</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="nav-Withdrawals" role="tabpanel"
                                        aria-labelledby="nav-Withdrawals-tab">
                                        <div class="transaction_status_grid Withdrawals">
                                            <div class="activity">
                                                <div class="svg_container">
                                                    <img src="img/icon/withdrawals.svg" alt="Withdrawals icon">
                                                        
                                                </div>
                                                <div class="transaction_title">
                                                    <p>Sponsorship Payout</p>
                                                    <span>Bank Transfer</span>
                                                </div>
                                            </div>
                                            <div class="amount">
                                                <p>- ₦30,000.00</p>
                                                <span>23 December 2019</span>
                                            </div>
                                        </div>
                                        <div class="transaction_status_grid Withdrawals">
                                            <div class="activity">
                                                <div class="svg_container">
                                                    <img src="img/icon/withdrawals.svg" alt="Withdrawals icon">
                                                        
                                                </div>
                                                <div class="transaction_title">
                                                    <p>Sponsorship Payout</p>
                                                    <span>Bank Transfer</span>
                                                </div>
                                            </div>
                                            <div class="amount">
                                                <p>- ₦30,000.00</p>
                                                <span>23 December 2019</span>
                                            </div>
                                        </div>
                                        <div class="transaction_status_grid Withdrawals">
                                            <div class="activity">
                                                <div class="svg_container">
                                                    <img src="img/icon/withdrawals.svg" alt="Withdrawals icon">
                                                        
                                                </div>
                                                <div class="transaction_title">
                                                    <p>Sponsorship Payout</p>
                                                    <span>Bank Transfer</span>
                                                </div>
                                            </div>
                                            <div class="amount">
                                                <p>- ₦30,000.00</p>
                                                <span>23 December 2019</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="chart_container">
                                <p class="_title_">Overview</p>
                                <div class="chrt_cont">
                                    <div class="transaction" id="transaction"></div>
                                    <div class="overview_details">
                                        <div class="_item wallet">
                                            <p>Wallet Top-up</p>
                                            <span>₦330,000.00</span>
                                        </div>
                                        <div class="_item payout">
                                            <p>Payouts</p>
                                            <span>₦330,000.00</span>
                                        </div>
                                        <div class="_item withdrawals">
                                            <p>Withdrawals</p>
                                            <span>₦330,000.00</span>
                                        </div>
                                        <div class="download">
                                            <button>Download History (PDF)</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                       </div>
                    </div>
                </div>
                
            </div>
        </section>

@endsection



