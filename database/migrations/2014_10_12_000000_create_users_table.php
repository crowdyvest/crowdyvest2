<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable(); 
            $table->string('phone')->nullable();
            $table->unsignedBigInteger('consent')->nullable();
            $table->unsignedBigInteger('designation')->nullable();
            $table->unsignedBigInteger('role_id')->nullable();
            $table->unsignedBigInteger('status_id')->nullable(); 
            $table->unsignedBigInteger('verified')->nullable();  
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('verified')->references('id')->on('statuses')->onDelete('set null')->nullable();
            $table->foreign('status_id')->references('id')->on('statuses')->onDelete('set null')->nullable(); 
            $table->foreign('designation')->references('id')->on('statuses')->onDelete('set null')->nullable();
            $table->foreign('consent')->references('id')->on('statuses')->onDelete('set null')->nullable();
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('set null')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
