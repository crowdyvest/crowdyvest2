<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->bigIncrements('id');  
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('date_of_birth')->nullable(); 
            $table->enum('gender', ['', 'Male', 'Female'])->nullable(); //Male, Female
            $table->string('phone_number')->nullable(); 
            $table->string('nationality')->nullable(); 
            $table->string('occupation')->nullable(); 
            $table->unsignedBigInteger('country_id')->nullable();
            $table->string('state_id')->nullable();
            $table->string('city_id')->nullable();
            $table->string('propix')->nullable();  
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null')->nullable();
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('set null')->nullable();
            // $table->foreign('state_id')->references('id')->on('states')->onDelete('set null')->nullable();
            // $table->foreign('city_id')->references('id')->on('cities')->onDelete('set null')->nullable();

 

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
