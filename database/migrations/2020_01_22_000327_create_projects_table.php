<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->unsignedBigInteger('status_id')->nullable();
            $table->unsignedBigInteger('plan_id')->nullable();
            $table->integer('publish')->nullable();
            $table->integer('cycle')->nullable();
            $table->unsignedBigInteger('company_id')->nullable();
            $table->unsignedBigInteger('insurance_provider_id')->nullable();
            $table->unsignedBigInteger('project_type_id')->nullable();
            $table->unsignedBigInteger('state_id')->nullable();
            $table->unsignedBigInteger('local_government_id')->nullable();
            $table->integer('harvest_period')->nullable();
            $table->integer('total_unit')->nullable();
            $table->integer('unit_left')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->double('price_per_unit')->nullable();
            $table->string('slug')->nullable();
            $table->string('image')->nullable();
            $table->longText('description')->nullable();
            $table->unsignedBigInteger('feature')->nullable();

            $table->timestamps();

            $table->foreign('company_id')->references('id')->on('companies')->onDelete('set null')->nullable();
            $table->foreign('insurance_provider_id')->references('id')->on('insurance_providers')->onDelete('set null')->nullable();
            $table->foreign('project_type_id')->references('id')->on('project_types')->onDelete('set null')->nullable();
            $table->foreign('state_id')->references('id')->on('states')->onDelete('set null')->nullable();
            $table->foreign('local_government_id')->references('id')->on('local_governments')->onDelete('set null')->nullable();
            $table->foreign('plan_id')->references('id')->on('plans')->onDelete('set null')->nullable();
            $table->foreign('status_id')->references('id')->on('statuses')->onDelete('set null')->nullable();
            $table->foreign('feature')->references('id')->on('statuses')->onDelete('set null')->nullable();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
