<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectRoiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('project_roi', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->unsignedBigInteger('project_id')->nullable();
        //     $table->unsignedBigInteger('roi_id')->nullable();
        //     $table->timestamps();

        //     $table->foreign('project_id')->references('id')->on('projects')->onDelete('set null')->nullable();
        //     $table->foreign('roi_id')->references('id')->on('roi')->onDelete('set null')->nullable();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_roi');
    }
}
