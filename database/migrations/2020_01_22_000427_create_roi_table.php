<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roi', function (Blueprint $table) {
            $table->bigIncrements('id');
             $table->unsignedBigInteger('project_id')->nullable();
            $table->double('percentage');
            $table->timestamps();
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('set null')->nullable(); 

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roi');
    }
}
