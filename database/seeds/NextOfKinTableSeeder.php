<?php

use Illuminate\Database\Seeder;
use App\Models\NextOfKin;

class NextOfKinTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $oldNextOfKin = NextOfKin::moveOldNOKUsers();
    }
}
