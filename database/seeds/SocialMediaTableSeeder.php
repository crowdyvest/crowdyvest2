<?php

use Illuminate\Database\Seeder;
// use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Profile;

class SocialMediaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $socialmedia = [ 
            ["id" => 1, "name" => "Facebook",],  
            ["id" => 2, "name" => "Twitter",],
            ["id" => 3, "name" => "Instagram",],
            ["id" => 4, "name" => "LinkedIn",],  
        ];

        foreach($socialmedia as $medium) {
            DB::table('social_media')->insert([
                'name' => $medium['name'],
            ]);
        }
    }
}
