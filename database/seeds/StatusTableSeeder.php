<?php

use Illuminate\Database\Seeder;
use App\Models\Status;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
            ['id' => 5, 'name' => 'Unassigned',],
            ["id" => 1, "name" => "Active",],
            ['id' => 2, 'name' => 'Inactive',],
            ['id' => 3, 'name' => 'Deactivated',],
            ['id' => 4, 'name' => 'Dormant',],
            ['id' => 6, 'name' => 'Unpaid',],
            ['id' => 7, 'name' => 'Paid',],
            ['id' => 8, 'name' => 'Complete',],
            ['id' => 9, 'name' => 'Opening Soon',],
            ['id' => 10, 'name' => 'Now Selling',],
            ['id' => 11, 'name' => 'Sold Out',],
            ['id' => 12, 'name' => 'Locked',],
            ['id' => 13, 'name' => 'Unlocked',],


        ];

        foreach ($statuses as $status) {

            $statusObject = new Status();
            $statusObject->id = (int) $status["id"];
            $statusObject->name = $status["name"];
            $statusObject->save();
        }
    }
}
