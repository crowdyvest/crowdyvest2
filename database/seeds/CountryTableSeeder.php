<?php

use Illuminate\Database\Seeder;
use App\Models\Country;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = Country::getOldCountry(); 
        foreach ($countries as $country) {
            $countryObject = new Country();

            $countryObject->id = $country["id"]; 
            $countryObject->sortname = $country["sortname"];
            $countryObject->name = $country["name"];
            $countryObject->phonecode = $country["phonecode"];
            $countryObject->created_at = $country["created_at"];
            $countryObject->updated_at = $country["updated_at"];

            $countryObject->save();
        }
    }
}
