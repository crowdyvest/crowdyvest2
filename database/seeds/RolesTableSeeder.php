<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {  
        
        $superAdmin = ["id" => 1, "name" => "Super Admin",];
        $admin = ['id' => 2, 'name' => 'Admin',];
        $crm = array('id' => 3, 'name' => 'CRM', );
        $hr = ['id' => 4, 'name' => 'HR',];
        $marketing = ['id' => 5, 'name' => 'Markeing',];
        // $notAssigned = ["id" => 0, "name" => "Not Assigned",];$notAssigned, 

        $roles = [$superAdmin, $admin, $crm, $hr, $marketing, ];

        foreach ($roles as $role) { 
            $roleObject = new Role();
            $roleObject->id = $role["id"];
            $roleObject->name = $role["name"]; 
            $roleObject->save();
        } 
    }
}
