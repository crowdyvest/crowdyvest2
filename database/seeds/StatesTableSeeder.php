<?php

use Illuminate\Database\Seeder;
use App\Models\State;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = State::getOldStates(); 
        foreach ($states as $state) {
            $countryObject = new State(); 
            $countryObject->id = $state->id;  
            $countryObject->name = $state->name; 
            $countryObject->country_id = $state->country_id; 
            $countryObject->created_at = $state->created_at;
            $countryObject->updated_at = $state->updated_at;

            $countryObject->save();
        } 
    }
}
