<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(
            [
                RolesTableSeeder::class,
                StatusTableSeeder::class,
                GroupTableSeeder::class,
                DesignationTableSeeder::class,
                CountryTableSeeder::class,
                StatesTableSeeder::class,
                UsersTableSeeder::class,
                ProfilesTableSeeder::class,
                BanksTableSeeder::class,
                UserBanksTableSeeder::class,
                NextOfKinTableSeeder::class,
                SocialMediaTableSeeder::class,
                UserSocialMediaTableSeeder::class,
                CompanyTableSeeder::class,
                PlansTableSeeder::class,
                FrequenciesTableSeeder::class,
                PaymentGatewayTableSeeder::class,
                CompanyPaymentGatewayTableSeeder::class,
                ProjectTypesTableSeeder::class,
                InsuranceProvidersTableSeeder::class,
                LocalGovernmentTableSeeder::class,
                ProjectsTableSeeder::class,
                PaymentsTableSeeder::class,
                SponsorshipsTableSeeder::class,
                CartsTableSeeder::class,
                TransactionsTableSeeder::class,
                ProductsTableSeeder::class,
                SavingsTableSeeder::class,

            ]
        );
    }
}
