<?php

use Illuminate\Database\Seeder;
use App\Models\LocalGovernment;

class LocalGovernmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $oldLGs = LocalGovernment::moveOldLocalGovernments();         
    }
}
