<?php

use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            ["id" => 1, "name" => "SaveVault", "description" => "Set up your savings plan for a minimum of 3 months and earn decent returns on your savings.",],
            ["id" => 2, "name" => "SaveFlex", "description" => "Start saving on your own terms with no restrictions, earn decent interest while at it.",],
            ["id" => 3, "name" => "SaveTribe", "description" => "Save funds with a Tribe for a period and earn good returns on your savings.",],
        ];

        foreach ($products as $product) {
            $productObject = new Product();
            $productObject->id = $product['id'];
            $productObject->name = $product['name'];
            $productObject->description = $product['description'];

            $productObject->save();
        }
    }
}
