<?php

use Illuminate\Database\Seeder;
use App\Models\Savings;
use Carbon\Carbon;

class SavingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $savings = new Savings();

        $savings->id = 1;
        $savings->plan_id = 1;
        $savings->product_id = 1;
        $savings->name = 'My Land';
        $savings->amount = 100.00;
        $savings->frequency_id = 2;
        $savings->start_date = Carbon::today();
        $savings->end_date = Carbon::today()->addMonths(3);
        $savings->status_id = 12;

        $savings->save();
    }
}
