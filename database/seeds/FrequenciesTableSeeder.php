<?php

use Illuminate\Database\Seeder;

use App\Models\Frequency;

class FrequenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //


        $frequencies = [
            ["id" => 1, "name" => "Daily", "description" => "Description for Daily",],
            ["id" => 2, "name" => "Weekly", "description" => "Description for Weekly",],
            ["id" => 3, "name" => "Monthly", "description" => "Description for Monthly",],
            ["id" => 4, "name" => "Quarterly", "description" => "Description for Quarterly",],
            ["id" => 5, "name" => "Anytime", "description" => "Description for Anytime",],
        ];

        foreach ($frequencies as $frequency) {
            $frequencyObject = new Frequency();

            $frequencyObject->id = $frequency['id'];
            $frequencyObject->name = $frequency['name'];
            $frequencyObject->description = $frequency['description'];

            $frequencyObject->save();
        }
    }
}
