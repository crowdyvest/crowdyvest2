<?php

use Illuminate\Database\Seeder;
use App\Models\Sponsorship;


class SponsorshipsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seeder = Sponsorship::moveOldSponsorships();
    }
}
