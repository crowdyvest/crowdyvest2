<?php

use Illuminate\Database\Seeder;
use App\Models\Designation;

class DesignationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $designations = [ 
            ["id" => 1, "name" => "Admin", "group" => 1,],  
            ["id" => 2, "name" => "Super Admin", "group" => 1,],  
        ];

        foreach ($designations as $designation) {

            $designationObject = new Designation();
            $designationObject->id = (int) $designation["id"];
            $designationObject->name = $designation["name"];  
            $designationObject->save();
        } 
    }
}
