<?php

use Illuminate\Database\Seeder;

use App\Models\Plan;

class PlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plans = [
            ["id" => 1, "name" => "Savings", "description" => "Description for Savings",],
            ["id" => 2, "name" => "Investment", "description" => "Description for Investment",],
        ];

        foreach ($plans as $plan) {
            $planObject = new Plan();
            $planObject->id = $plan['id'];
            $planObject->name = $plan['name'];
            $planObject->description = $plan['description'];

            $planObject->save();
        }

    }
}
