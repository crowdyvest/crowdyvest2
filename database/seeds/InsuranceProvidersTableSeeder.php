<?php

use Illuminate\Database\Seeder;
use App\Models\InsuranceProvider;


class InsuranceProvidersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $moveOldInsurance = InsuranceProvider::moveOldInsurance();
    }
}
