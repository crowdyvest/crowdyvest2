<?php

use Illuminate\Database\Seeder;
use App\Group;

class GroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groups = [ 
            ["id" => 1, "name" => "Read",],  
            ["id" => 2, "name" => "Write",],  
        ];

        foreach ($groups as $group) {

            $groupObject = new Group();
            $groupObject->id = (int) $group["id"];
            $groupObject->name = $group["name"];  
            $groupObject->save();
        } 
    }
}
