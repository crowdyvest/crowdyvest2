<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
 
use App\Models\UserSocialMedia;  

class UserSocialMediaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $oldUserSocialMedia = UserSocialMedia::moveOldUserSocialMedia(); 
    } 
}