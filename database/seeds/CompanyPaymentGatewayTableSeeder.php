<?php

use Illuminate\Database\Seeder;
use App\Models\Company;

class CompanyPaymentGatewayTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companyPaymentGatewayTableSeeder = Company::companyPaymentGatewayTableSeeder();
    }
}
