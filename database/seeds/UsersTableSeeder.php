<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $users = User::getOldUsers();  
       
    }

    private function getUserType(string $type): int {
        switch ($type){
            case 'rider':
                return 1;

            case 'driver':
                return 2;

            case 'undefined':
                return 6;
        }
    }
}
