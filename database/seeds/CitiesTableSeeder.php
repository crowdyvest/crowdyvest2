<?php

use Illuminate\Database\Seeder;
use App\Models\City;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = City::getOldCities(); 
        foreach ($cities as $city) {
            $cityObject = new City();
            // var_dump($city["state_id"]); 
            $cityObject->id = $city["id"];
            $cityObject->name = $city["name"]; 
            // $cityObject->state_id = $city["state_id"];  
            // $cityObject->created_at = $city["created_at"];
            // $cityObject->updated_at = $city["updated_at"];

            $cityObject->save();
        }
    }
}
