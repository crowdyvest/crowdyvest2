<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/about', 'HomeController@about_us')->name('about');

Route::get('/dashboard', 'UsersController@index')->name('dashboard');

Route::get('/project/details/{slug}', 'Web\ProjectsController@show')->name('projectdetails');

Route::get('/sponsorshop', ['as' => 'sponsorshop', 'uses' => 'Web\SponsorshipController@sponsorshop']);


Route::prefix('plan')->group(function () {
    Route::get('/savings', ['as' => 'savings', 'uses' => 'Web\SavingsController@index']);
    // All savings plans comes here
    Route::get('/sponsorships', 'UsersController@sponsorships')->name('sponsorships');
});
