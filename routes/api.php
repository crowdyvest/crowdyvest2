<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([ 'prefix' => 'auth' ], function () {
    Route::post('login', 'API\APIController@login');
    Route::post('register', 'API\APIController@register');

    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'API\APIController@logout');
        Route::get('user', 'AuthController@user');
    });
});


//Route::post('register', 'API\APIController@register');
//Route::post('login', 'API\APIController@login');
//Route::post('logout', 'API\APIController@logout');

//Route::middleware('api')->group( function() {
    /** Projects */
    Route::resource('projects', 'API\ProjectsController');
    Route::get('project/{project_id}', 'API\ProjectsController@projectById');
    Route::get('featured-projects', 'API\ProjectsController@featuredProjects');
    Route::get('project-types', 'API\ProjectTypesController@index');
    Route::get('project-type/{type_id}', 'API\ProjectTypesController@show');

    /** Carts */
    Route::get('carts', 'API\CartController@index');
    Route::get('cart/{cart_id}', 'API\CartController@show');
    Route::get('user_cart/{user_id}', 'API\CartController@cartByUser');
    Route::get('user_cart_item/{user_id}/{item_id}', 'API\CartController@cartItemsByUser');
    Route::get('item_exist_in_cart/{user_id}/{project_id}', 'API\CartController@itemExistInCart');
    Route::get('getcartwithdetails/{id}', 'API\CartController@getCartWithDetails');
    Route::post('add_item_to_cart', 'API\CartController@addItemToCart');
    Route::delete('cart/delete/{item_id}', 'API\CartController@deleteCartItem');
    Route::patch('cart/update', 'API\CartController@updateItemUnits');
    Route::post('cart/clear', 'API\CartController@clearCart');


    Route::get('sponsorships/{user_id?}', 'API\SponsorshipsController@index');
    Route::get('sponsorship/{id}', 'API\SponsorshipsController@show');

    /** Status */
    Route::get('statuses/{id?}', 'API\StatusController@index');

    /** Bank */
    Route::get('banks', 'API\BankController@index');
    Route::get('bank/{id?}', 'API\BankController@index');
    Route::get('banks/user/{user_id}', 'API\BankController@userBank');
    Route::post('banks/user', 'API\BankController@addUserBank');
    Route::patch('banks/user/update', 'API\BankController@updateUserBank');
    Route::delete('banks/user/{user_id}/delete/{user_bank_id}', 'API\BankController@deleteUserBank');

    /** Product */
    Route::get('products', 'API\ProductController@index');
    Route::get('product/{id?}', 'API\ProductController@index');
    Route::post('product/create', 'API\ProductController@createProduct');
    Route::patch('product/update', 'API\ProductController@updateProduct');
    Route::delete('product/delete/{id}', 'API\ProductController@deleteProduct');
//});



//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
