<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Models\Country;
use App\Models\State;

class Profile extends Model
{
    public static function getOldProfiles() {

        $profiles = Profile::on('fcdb')
            ->with('users')
            ->where('profiles.id', '>', 0)
            ->leftJoin('users', 'users.id', 'profiles.user_id')
            ->select(
                'profiles.id',
                'profiles.user_id',
                'profiles.date_of_birth',
                'profiles.gender',
                'profiles.phone_number',
                'profiles.nationality',
                'profiles.occupation',
                'profiles.country_id',
                'profiles.statepro_id',
                'profiles.city',
                'profiles.propix',
                'profiles.created_at',
                'profiles.updated_at',
            )
            ->orderBy('profiles.id')
            ->chunk(
                500,
                function($profiles) {
                    foreach($profiles as $profile) {
                        if(self::userIdExists($profile['user_id'])) {
                            DB::table('profiles')
                            ->insert(
                                [
                                    'user_id' => $profile['user_id'],
                                    'date_of_birth' => $profile['date_of_birth'],
                                    'gender' => self::getUserGender($profile['gender']),
                                    'phone_number' => $profile['phone_number'],
                                    'nationality' => $profile['nationality'],
                                    'occupation' => $profile['occupation'],
                                    'country_id' => self::countryByName($profile['country_id']),
                                    'state_id' => $profile['statepro_id'],
                                    'city_id' => $profile['city'],
                                    'propix' => $profile['propix'],
                                    'created_at' => $profile['created_at'],
                                    'updated_at' => $profile['updated_at'],
                                ]
                            );
                        }
                    }

                }
            );
    }

    public static function checkGenderFirstLetter($gender) {
        $firstLetter = substr($gender, 0, 1);
        if($firstLetter == 'm') {
            return 'Male';
        } elseif($firstLetter == 'f') {
            return 'Female';
        } elseif($firstLetter == 'M') {
            return 'Male';
        } elseif($firstLetter == 'F') {
            return 'Female';
        }

    }

    public static function getUserGender($gender) {
        switch ($gender) {
            case strlen($gender) > 0:
                $gender = self::checkGenderFirstLetter($gender);
                break;

            case strlen($gender) == 0:
                $gender = '';
                break;

            default:
                $gender = '';
                break;
        }
        return $gender;
    }

    public static function userIdExists($user_id) {
        $user_object = DB::table('users')->where('id', $user_id)->get();
        if(sizeof($user_object) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static function countryById($countryId) {
        $countryObject = Country::where('id', $countryId)->get('name');
        return $countryObject[0]->name;
    }

    public static function countryByName ($countryName) {
        if($countryName == '') {
            $countryName = 'Nigeria';
        }

        if(is_numeric($countryName)) {
            $countryName = self::countryById($countryName);
        }

        $countryObject = Country::where('name', $countryName)->get('id');
        if(sizeof($countryObject) > 0) {
            return $countryObject[0]->id;
        } else {
            return 160;
        }

    }

    public function users() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function states() {
        return $this->hasOne(State::class, 'state_id', 'id');
    }
}
