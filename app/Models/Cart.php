<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Project;
use App\User;


class Cart extends Model
{
    protected $fillable = [
        'user_id', 'project_id', 'units_sponsored',
    ];

    public static function moveOldCart() {
        $oldCarts = self::on('fcdb')->get();
        $counter = 0;
        foreach($oldCarts as $oldCart) {
//            var_dump($oldCart["user_id"]);
            if(Project::projectExists($oldCart["farm_id"]) && User::userExists($oldCart["user_id"])) {
                DB::table('carts')->insert([
                    'id' => $oldCart["id"],
                    'user_id' => $oldCart["user_id"],
                    'project_id' => $oldCart["farm_id"],
                    'units_sponsored' => $oldCart["sponsor_unit"],
                    'created_at' => $oldCart["created_at"],
                    'updated_at' => $oldCart["updated_at"],
                ]);
            } else {
                $counter = $counter + 1;
            }
        }

//        var_dump($counter);
    }

    public function project()
    {
        return $this->belongsTo(Project::class, 'projects', 'project_id', 'id');
    }

}
