<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\models\Sponsorship;
use App\Models\Savings;

class Plan extends Model
{
    //
    public function sponsorship()
    {
        return $this->hasMany(Sponsorship::class, 'plan_id', 'id');
    }

    public function savings()
    {
        return $this->hasMany(Savings::class, 'plan_id', 'id');
    }
}
