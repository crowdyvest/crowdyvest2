<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Project;

class ProjectType extends Model
{
    //
    public static function moveOldProjectType() {
        $oldProjectTypes = DB::connection('fcdb')->table('farmtypes')->where('id', '>', 0)->get();

        foreach($oldProjectTypes as $oldProjectType) {
            DB::table('project_types')->insert([
                'id' => $oldProjectType->id,
                'name' => $oldProjectType->farmtype,
                'description' => $oldProjectType->content,
                'created_at' => $oldProjectType->created_at,
                'updated_at' => $oldProjectType->updated_at,
            ]);

        }

    }

    public function projects() {
        return $this->belongsTo(Project::class);
    }

}
