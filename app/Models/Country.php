<?php

namespace App\Models; 

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Country extends Model
{
    public static function getOldCountry() { 
        $countries = self::on('fcdb')->where('id', '>', 0)->get(); 
 
        return $countries;  
    }
}
