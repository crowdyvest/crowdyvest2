<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class InsuranceProvider extends Model
{
    //
    public static function moveOldInsurance() {
        $oldInsurances = DB::connection('fcdb')->table('insurances')->where('id', '>', 0)->get();
        foreach ($oldInsurances as $oldInsurance) {
            // var_dump($oldInsurance); 
           DB::table('insurance_providers')->insert([
            'id' => $oldInsurance->id,
            'title' => $oldInsurance->title,
            'logo' => $oldInsurance->logo,
            'description' => $oldInsurance->description,
            'created_at' => $oldInsurance->created_at,
            'updated_at' => $oldInsurance->updated_at,
           ]);
        }
    }
}
