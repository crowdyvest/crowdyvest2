<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Company;


class PaymentGateway extends Model
{
    //
    public static function seedPaymentGatewayTable() {
        $gateways = [
            ['name' => 'Paystack', 'description' => 'Modern online and offline payments for Africa Paystack helps businesses in Africa get paid by anyone, anywhere in the world'],
            ['name' => 'Monnify', 'description' => 'Monnify is a suite of products that help organizations manage important aspects of the running of their businesses.'],
            ['name' => 'Mastercard', 'description' => 'Mastercard is a unified payment system, the largest in Africa'],
            ['name' => 'Paypal', 'description' => 'Paypal is a unified payment system, the largest wallet system in the world'],
        ];

        foreach($gateways as $gateway) {
            DB::table('payment_gateways')->insert([
                'name' => $gateway['name'],
                'description' => $gateway['description'],
            ]);
        }
    }

    public function company() {
        return $this->belongsToMany(Company::class);
    }

    public static function paymentGatewayByName($name) { 
        if($name == 'Paystack') {
            $id = 1;
        } elseif($name == 'paystack') { 
            $id = 1;
        } elseif($name == 'Monnify') { 
            $id = 2;
        } elseif($name == 'Mastercard') {
            $id = 3; 
        } elseif($name == 'MasterCard') {
            $id = 3; 
        } elseif($name == 'Paypal') {
            $id = 4; 
        } else {
            $id = 1;
        }
        
        return $id;
    }
}
