<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class UserSocialMedia extends Model
{

    public static function moveOldUserSocialMedia() {
        $oldUserSocialMedia = Profile::on('fcdb')
            ->where('id', '>', 0)
            ->orderBy('user_id')
            ->select(
                'user_id', 
                'facebook',
                'instagram',
                'twitter', 
                'linkedin',
                )
            ->get();  

        foreach($oldUserSocialMedia as $oldUserSocialMedium) {  
            if(Profile::userIdExists($oldUserSocialMedium['user_id'])) {
                $facebook = self::storeUserSocialMediaAccount($oldUserSocialMedium['user_id'], 1, $oldUserSocialMedium['facebook']);
                $instagram = self::storeUserSocialMediaAccount($oldUserSocialMedium['user_id'], 2, $oldUserSocialMedium['instagram']);
                $twitter = self::storeUserSocialMediaAccount($oldUserSocialMedium['user_id'], 3, $oldUserSocialMedium['twitter']);
                $linkedin = self::storeUserSocialMediaAccount($oldUserSocialMedium['user_id'], 4, $oldUserSocialMedium['linkedin']);
                
            }
            
        } 

    }

    public static function storeUserSocialMediaAccount($user_id, $social_media_id, $username) {
        if(!is_null($username)) {
            DB::table('user_social_media')
                ->insert(
                    [
                        'user_id' => $user_id,
                        'social_media_id' => $social_media_id,
                        'username' => $username, 
                    ]
                );
        }
        
    } 

    public function users() {
        return $this->belongsTo(User::class);
    }
}
