<?php

namespace App\models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use App\User;
use App\Models\PaymentGateway;
use App\Models\Profile;
use App\Models\Payment;
use App\Models\Project;

class Sponsorship extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function plan()
    {
        return $this->belongsTo(Plan::class, 'plan_id', 'id');
    }
    /**
     * @param $payment_id
     * @return mixed
     */
    private static function checkValidPayment($payment_id) {
        return Payment::where('id', $payment_id)->exists();
    }


    /**
     * @return bool
     */
    public static function moveOldSponsorships() {
        $oldSponsorships = Sponsorship::on('fcdb')->get();
        $counter = 0;
        $counted = 0;
        $failed = [];
        foreach ($oldSponsorships as $sponsorship) {
            if(Profile::userIdExists($sponsorship->user_id)) {
                $counted = $counted + 1;
                DB::table('sponsorships')->insert([

                    'id' => $sponsorship->id,
                    'user_id' => $sponsorship->user_id,
                    'project_id' => $sponsorship->farm_id,
                    'payment_id' => $sponsorship->payment_id == 0 ? 1 : $sponsorship->payment_id,
                    'repayment_status_id' => self::sponsorshipStatus($sponsorship->repayment_status),
                    'units_sponsored' => $sponsorship->sponsor_unit,
                    'complete_profile' => $sponsorship->complete_profile,
                    'start_date' => $sponsorship->start_date,
                    'end_date' => $sponsorship->end_date,
                    'created_at' => $sponsorship->created_at,
                    'updated_at' => $sponsorship->updated_at,
                ]);
            }
        }

        return true;
    }

    /**
     * @param $status
     * @return int
     */
    private static function sponsorshipStatus($status) {
        if($status == 'paid') {
            $status_id = 7;
        } else {
            $status_id = 6;
        }

        return $status_id;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project() {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }




    /**
     * @param $sponsorship_id
     * @return int
     *
     * (end_date - today / end_data - start_date) * 100
     */
    public static function sponsorshipPercentageCompletion($sponsorship_id) {
        $sponsorship = Sponsorship::where('id', $sponsorship_id)->first();

        $start_time = Carbon::parse($sponsorship->start_date);
        $finish_time = Carbon::parse($sponsorship->end_date);
        $today = \Carbon\Carbon::now();

        if($today->greaterThanOrEqualTo($finish_time)) {
            $numerator = $start_time->diffInDays($finish_time, false);
            $denominator = $today->diffInDays($finish_time, false);
            return 100 - ((int) ($denominator/$numerator) * 100);
        } else {
            return 100;
        }


    }


}
