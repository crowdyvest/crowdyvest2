<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Frequency extends Model
{
    public function savings()
    {
        return $this->belongsToMany(Savings::class, 'frequencies','frequency_id','id');
    }
}
