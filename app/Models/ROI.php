<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ROI extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'roi';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function project() {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }
}
