<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    //getOldCities
    public static function getOldCities() { 
        $states = self::on('fcdb')->where('id', '>', 0)->get(); 
 
        return $states;  
    }
}
