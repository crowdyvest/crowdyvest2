<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\Profile;
use App\Models\Bank;
use App\User;

class UserBank extends Model
{
     protected $fillable = [
         'user_id', 'bank_id', 'account_name', 'account_number', 'bvn'
     ];


    public function banks() {
        return $this->hasMany(Bank::class, 'bank_id', 'id');
    }

    public static function moveOldUserBanks() {
        $oldUserBanks = Profile::on('fcdb')
            ->where('id', '>', 0)
            ->orderBy('user_id')
            ->select('user_id', 'bank_id', 'acct_name', 'acct_number', 'bvn', 'created_at', 'updated_at')
            ->get();


        foreach($oldUserBanks as $oldUserBank) {
            if(Profile::userIdExists($oldUserBank['user_id'])) {
                DB::table('user_banks')
                    ->insert([
                        'user_id' => trim($oldUserBank['user_id']),
                        'bank_id' => trim($oldUserBank['bank_id']),
                        'account_name' => trim($oldUserBank['acct_name']),
                        'account_number' => is_null($oldUserBank['acct_number']) ? '' : trim($oldUserBank['acct_number']),
                        'bvn' => trim($oldUserBank['bvn']),
                        'created_at' => $oldUserBank['created_at'],
                        'updated_at' => $oldUserBank['updated_at'],
                    ]);
            }

        }
    }
}
