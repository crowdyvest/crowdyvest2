<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\State;

class LocalGovernment extends Model
{
    //
    public function states() {
        return $this->belongsTo(State::class);
    }

    public static function moveOldLocalGovernments() {
        $oldLGs = DB::connection('fcdb')->table('locals')->get();

        foreach($oldLGs as $oldLG) {
            // var_dump($oldLG->local_id);
            DB::table('local_governments')->insert([
                'id' => $oldLG->local_id,
                'state_id' => $oldLG->state_id,
                'name' => $oldLG->local_name, 
            ]);
        }
    }
}
