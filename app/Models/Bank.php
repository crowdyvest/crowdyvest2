<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\UserBank;

class Bank extends Model
{
    //
    public static function moveOldBanks() {
        $banks = Bank::on('fcdb')->where('id', '>', 0)->get();

        foreach($banks as $bank) {
            DB::table('banks')->insert([
                'name' => $bank['bank_name'],
                'created_at' => $bank['created_at'],
                'updated_at' => $bank['updated_at'],
            ]);
        }
    }

    public function users() {
        return $this->belongsTo(UserBank::class);
    }
}
