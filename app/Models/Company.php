<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use App\Models\PaymentGateway;


class Company extends Model
{
    public function daily_toast()
    {
       return $this->daily()->select(‘toast’, DB::raw('count(*) as daily_toast'))->get();
    }

    public function paymentGateway() {
        return $this->hasMany(PaymentGateway::class);
    }

    public function project() {
        return $this->hasMany('App\Models\Project');
    }

    public static function moveOldCompany() {
        $oldCompanies = self::on('fcdb')->where('id', '>', 0)->get();

        foreach($oldCompanies as $oldCompany) {
            DB::table('companies')->insert([
                'title' => $oldCompany['title'],
                'description' => $oldCompany['description'],
                'logo' => $oldCompany['logo'],
            ]);
        }
    }

    public static function companyPaymentGatewayTableSeeder() {
        $companyGateways = Company::on('fcdb')->select('id', 'paystack_code', 'monnify_code')->get();

        foreach($companyGateways as $companyGateway) {
            self::storeCompanyPaymentGateway($companyGateway['id'], 1, $companyGateway['paystack_code']);
            self::storeCompanyPaymentGateway($companyGateway['id'], 2, $companyGateway['monnify_code']);
        }
    }

    private static function storeCompanyPaymentGateway($company_id, $payment_gateway_id, $code) {
        DB::table('company_payment_gateways')->insert([
            'company_id' => $company_id,
            'payment_gateway_id' => $payment_gateway_id,
            'code' => $code,
        ]);
    }


}
