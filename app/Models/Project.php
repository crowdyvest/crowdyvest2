<?php

namespace App\Models;

use App\Models\ProjectType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\ProjectROI;
use App\models\Sponsorship;
use App\Models\Plan;

class Project extends Model
{
    public static function moveOldProjects() {
        $old_projects = DB::connection('fcdb')->table('farms')->get();
        foreach($old_projects as $op) {
            DB::table('projects')->insert([
                'id' => $op->id,
                'title' => $op->farm_title,
                'cycle' => $op->farm_cycle,
                'plan_id' => 2,
                'harvest_period' => $op->harvest_period,
                'total_unit' => $op->total_unit,
                'unit_left' => $op->unit_left,
                'end_date' => $op->end_date,
                'start_date' => $op->start_date,
                'publish' => $op->publish,
                'price_per_unit' => $op->price_perunit,
                'slug' => $op->slug,
                'image' => $op->farm_image,
                'description' => $op->farm_description,
                'feature' => $op->feat == 0 ? 2 : $op->feat,
                'company_id' => $op->company_id,
                'insurance_provider_id' => $op->insurance_id,
                'project_type_id' => $op->farmtype_id,
                'state_id' => $op->state_id,
                'local_government_id' => $op->local_id,
                'status_id' => $op->status_id,
                'created_at' => $op->created_at,
                'updated_at' => $op->updated_at
            ]);

            DB::table('roi')->insert([
                'project_id' => $op->id,
                'percentage' => $op->roi,
            ]);
        }
    }

    public static function project_exists(int $project_id)
    {
        return is_null(self::where('id', $project_id)->first());
    }

    public static function projectExists($project_id) {
        return DB::connection('fcdb')->table('farms')->where('id', $project_id)->first();
    }

    public static function featuredProjects() {
        return self::where('feature', 1)->get();
    }

    public function project_type() {
        return $this->belongsTo(ProjectType::class);
    }

    public function project_roi()  {
        return $this->belongsToMany(ProjectROI::class);
    }

    public function sponsorship() {
        return $this->HasMany(Sponsorship::class, 'project_id', 'id');
    }

    public function roi() {
        return $this->hasOne(ROI::class, 'project_id', 'id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id', 'id');
    }
}
