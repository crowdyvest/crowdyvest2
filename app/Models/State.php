<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\LocalGovernment;

class State extends Model
{ 
    public static function getOldStates() { 
        $states = self::on('fcdb')->get(); 
 
        return $states;  
    }

    public function local_governments() {
        return $this->hasMany(LocalGovernment::class);
    }
}
