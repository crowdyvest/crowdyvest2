<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Profile;

class NextOfKin extends Model
{
    public function users() {
        return $this->belongsTo('App/User');
    }

    public static function moveOldNOKUsers() {
        $oldUserNOKs = Profile::on('fcdb')
            ->where('id', '>', 0)
            ->orderBy('user_id')
            ->select(
                'user_id',
                'nextkin_fname',
                'nextkin_sname',
                'nextkin_relationship',
                'nextkin_email',
                'nextkin_mobile',
                'nextkin_address',
                'created_at',
                'updated_at'
                )
            ->get();

        foreach($oldUserNOKs as $oldUserNOK) {
            if(Profile::userIdExists($oldUserNOK['user_id'])) {
                DB::table('next_of_kins')
                    ->insert([
                        'user_id' => trim($oldUserNOK['user_id']),
                        'first_name' => trim($oldUserNOK['nextkin_fname']),
                        'last_name' => trim($oldUserNOK['nextkin_sname']),
                        'relationship' => trim($oldUserNOK['nextkin_relationship']),
                        'email' => trim($oldUserNOK['nextkin_email']),
                        'phone' => trim($oldUserNOK['nextkin_mobile']),
                        'address' => trim($oldUserNOK['nextkin_address']),
                        'created_at' => $oldUserNOK['created_at'],
                        'updated_at' => $oldUserNOK['updated_at'],
                    ]);
            }

        }
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
