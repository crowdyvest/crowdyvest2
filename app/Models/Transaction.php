<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use App\Models\Project;


class Transaction extends Model
{
    public static function moveOldTransaction() {
        $oldTransactions = self::on('fcdb')->get();

        foreach($oldTransactions as $transactions) {
            if(!Project::project_exists($transactions["farm_id"]) && !Payment::payment_exists($transactions['payment_id'])) {
                DB::table('transactions')->insert([
                    'id' => $transactions["id"],
                    'user_id' => $transactions["user_id"],
                    'payment_id' => $transactions["payment_id"],
                    'project_id' => $transactions["farm_id"],
                    'units_sponsored' => $transactions["sponsor_unit"],
                    'created_at' => $transactions["created_at"],
                    'updated_at' => $transactions["updated_at"],
                ]);
            }

        }
    }

}
