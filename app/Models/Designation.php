<?php

namespace App\Models; 

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    /**
     * Get the group record associated with the designation.
     */
    public function group()
    {
        return $this->hasMany('App\Group');
    }
}
