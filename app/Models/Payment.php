<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use App\Models\User;
use App\Models\PaymentGateway;
use App\Models\Profile;
use App\Models\Sponsorship;

class Payment extends Model
{
    //
    public function users() {
        return $this->belongsTo(User::class);
    }

    public function sponsorships() {
        return $this->hasMany(Sponsorship::class);
    }

    public static function payment_exists($payment_id)
    {
        return is_null(self::where('id', $payment_id)->first());
    }


    public static function moveOldPayments() {
        $oldPayments = Payment::on('fcdb')->get();
        foreach($oldPayments as $payment) {
            if(Profile::userIdExists($payment['user_id'])) {
                DB::table('payments')->insert([
                    'id' => $payment->id,
                    'user_id' => $payment->user_id,
                    'total_payment' => (double) $payment->totalpayment,
                    'payment_gateway_id' => PaymentGateway::paymentGatewayByName($payment->gateway),
                    'gateway_status' => $payment->gateway_status,
                    'gateway_reference' => $payment->gateway_reference,
                    'gateway_transaction' => $payment->gateway_transaction,
                    'created_at' => $payment->created_at,
                    'updated_at' => $payment->updated_at,
                ]);
            }
        }
    }


}
