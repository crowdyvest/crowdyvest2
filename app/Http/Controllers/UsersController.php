<?php

namespace App\Http\Controllers;

use App\Http\Resources\Sponsorship as SponsorshipResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\ProjectType;
use App\Models\Project;
use App\Models\Sponsorship;
use Illuminate\Support\Facades\Auth;
use App\Models\ROI;
use App\Http\Controllers\Web\ProjectsController;


class UsersController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::id();
        $featuredProjects = app('App\Http\Controllers\Web\ProjectsController')
            ->projectsDetails(Project::featuredProjects());
        return view('cvusers.dashboard',
            compact('featuredProjects', )
        );
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sponsorships() {
        $sponsorships = Sponsorship::where('user_id', Auth::user()->id)
            ->orderBy('start_date', 'DESC')
            ->get();

        return view('cvusers.sponsorships', compact('sponsorships',));
    }


}
