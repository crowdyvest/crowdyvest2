<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Bank;
use App\Models\UserBank;
use Illuminate\Support\Facades\Validator;

class BankController extends Controller
{
    public function index(int $id = null)
    {
        if (!is_null($id)) {
            $bank = Bank::findOrFail($id);
            if (empty($bank)) {
                return response()->json(['message' => 'No Bank with the ID '.$id], 400);
            } else {
                return response()->json(['data' => $bank,], 200);
            }
        } else {
            $banks = Bank::pluck('id')->get();
            return response()->json(['banks' => $banks], 200);
        }
    }

    public function userBank(int $user_id)
    {
        $userBank = UserBank::where('user_id', $user_id)->get();

        return response()->json(['data' => $userBank,], 200);
    }

    public function addUserBank(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'user_id' => ['required', 'integer'],
            'bank_id' => ['required', 'integer', 'max:255'],
            'account_name' => ['required', 'string', 'max:255'],
            'account_number' => ['required', 'string', 'max:15'],
            'bvn' => ['required', 'integer', 'min:9',],
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $userbank = UserBank::create([
            'user_id' => $request->post('user_id'),
            'bank_id' => $request->post('bank_id'),
            'account_name' => $request->post('account_name'),
            'account_number' => $request->post('account_number'),
            'bvn' => $request->post('bvn'),
        ]);

        if($userbank) {
            return response()->json(['message' => 'User Bank Added Successfully.'], 200);
        } else {
            return response()->json(['error' => 'Failed to Add User Bank, check Internet connection',], 500);
        }
    }

    public function updateUserBank(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'id' => ['required', 'integer',],
            'bank_id' => ['required', 'integer',],
            'account_name' => ['required', 'string',],
            'account_number' => ['required', 'string',],
            'bvn' => ['required', 'integer', 'min:9',],
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $updateuserbank = UserBank::where('id', $request->post('id'))
            ->update(
                [
                    'bank_id' => $request->post('bank_id'),
                    'account_name' => $request->post('account_name'),
                    'account_number' => $request->post('account_number'),
                    'bvn' => $request->post('bvn'),
                ]
            );

        if ($updateuserbank) {
            return response()->json(['message' => 'User Bank updated Successfully.'], 200);
        } else {
            return response()->json(['error' => 'Failed to update User Bank, check Internet connection',], 500);
        }

    }

    public function deleteUserBank(int $user_id, int $user_bank_id)
    {
        $deleteBank = UserBank::where('id', $user_bank_id)->where('user_id', $user_id)->delete();

        if($deleteBank) {
            return response()->json(
                ['message' => 'User Bank deleted successfully.',],
                200);
        } else {
            return response()->json(
                ['message' => 'Unsuccessful. Please check your internet connection.'],
                500);

        }
    }
}
