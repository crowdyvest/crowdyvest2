<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Validator;

class RegisterController extends Controller
{
    public function register(Request $request) {

//        $request['password_confirmation'] = $request['password'];
//        $validator =  Validator::make($request->all(), [
//            'first_name' => ['required', 'string', 'max:255'],
//            'last_name' => ['required', 'string', 'max:255'],
//            'phone_number' => ['required', 'string', 'max:255'],
//            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
//            'password' => ['required', 'string', 'min:6', 'confirmed'],
//            'password_confirmation' => ['required'],
//        ]);
//
//        if($validator->fails()){
//            return $this->sendError('Validation Error.', $validator->errors());
//        }
//
//        $input = $request->all();
//        $input['password'] = Hash::make($input['password']);
//        $user = User::create($input);
//        $success['token'] =  $user->createToken('CVV2')->accessToken;
//        $success['name'] =  $user->first_name.'-'.$user->last_name;
//
//        return $this->sendResponse($success, 'User register successfully.');
    }


    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
//    public function login(Request $request){
//        $email = $request->email;
//        $password = $request->password;
//        //......................................................................................................................
//        $isEmpty = ['email'=>$email, 'password'=>$password];
//
//        if(Controller::checkEmptyFields($isEmpty)) return Controller::sendError('Unauthorised.', ['error'=>Controller::checkEmptyFields($isEmpty)]);
////        if(Controller::checkEmptyFields($isEmpty))return jsonResponse(false, checkEmptyFields($isEmpty));
//        if(is_null($email))return Controller::sendError('invalid', 'Invalid email address entered.');
////        if(strlen($phone_number) > 11 || strlen($phone_number) < 11)return Controller::sendError('invalid', 'Phone number should contain only 11 digits.');
////
////        $formattedPhone = '+234'.(int)$phone_number;//convert to Nigeria
//        $loginData = ['email' => $email, 'password' => $password];
//        //......................................................................................................................
//        //validates the login credentials
//        if(!Auth::attempt($loginData))return Controller::sendError('Unauthorised', 'Invalid login credentials.');
//        //
//        $user =  User::where('email', $email)->first();
////
//        //create a new access token when login is successful
//        $access_token = Controller::createAccessToken($user);
//        return $access_token;
////        if(!$Bus)return jsonResponse(false, 'Sorry, it seems like there is no bus attached to this account.');
////        $VA->bus = $Bus;
////        $data = ['access_token'=>$access_token, 'user'=>$VA];
////
////        //......................................................................................................................
////        //deletes any other access tokens from the database already created before this.
////        AccessToken::deleteEarliestUserAccessToken($VA->id);
////
////        return jsonResponse(true, 'Login successful', 200, $data);
//    }
    public function login(Request $request)
    {
//        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
//            $user = Auth::user();
//            $success['token'] =  $user->createToken('CVV2')-> accessToken;
//            $success['user'] =  [
//                'id' => $user->id,
//                'first_name' => $user->first_name,
//                'last_name' => $user->last_name,
//                'email' => $user->email,
//            ];
//
//
//            return $this->sendResponse($success, 'User login successfully.');
//        }
//        else{
//            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
//        }
    }

    /**
     * Logout Api
     *
     * @return \Illuminate\Http\Response
     */

    public function logout()
    {
        if (Auth::check()) {
           $logout = Auth::user()->AauthAcessToken()->delete();

           return $this->sendResponse($logout, 'User logged out successfully.');
        }
        else{
            return $this->sendError('Unsuccessful.', ['error'=>'No user session found']);
        }
    }
}
