<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use Validator;

use App\Http\Resources\Project as ProjectResource;


use App\User;

use App\Models\Project;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param null $id
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::where('publish', 1)->orderBy('id', 'DESC');
        $projects = $projects->get();

        return Controller::sendResponse(ProjectResource::collection($projects), 'Projects retrieved successfully.');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function projectById(int $id = null) {
        if(!is_null($id)) {
            $projects = Project::where('publish', 1)->orderBy('id', 'DESC')->where('id', $id)->get();
            return Controller::sendResponse(ProjectResource::collection($projects), 'Projects retrieved successfully.');
        }

    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function featuredProjects() {
        $projects = Project::where('feature', 1)
            ->where('publish', 1)
            ->orderBy('id', 'DESC')
            ->get();

        return Controller::sendResponse(ProjectResource::collection($projects), 'Featured Projects retrieved successfully.');
    }
}
