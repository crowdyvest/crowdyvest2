<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\Project as ProjectResource;
use Illuminate\Http\Request;
use App\Models\Cart;
use Illuminate\Support\Facades\Validator;
use App\Models\Project;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function index(){
        $cart = Cart::all();

        return response()->json([
            'cart' => $cart,
        ],200);
    }

    public function show(int $cart_id) {
        $cart = Cart::where('id', $cart_id)->get();

        return response()->json(
            [
                'cart' => $cart,
                ],200
        );
    }

    public function cartByUser(int $user_id)
    {
        $user_cart = Cart::where('user_id', $user_id)->get();

        return response()->json(
            [
                'cart' => $user_cart,
                ],
        200);
    }

    public function cartItemsByUser($user_id, $item_id)
    {
        $items = Cart::where('user_id', $user_id)->where('id', $item_id)->get();

        return response()->json(
            [
                'items' => $items,
            ],
            200
        );
    }

    public function itemExistInCart(int $user_id, int $project_id) {
        return Cart::where('user_id', $user_id)
            ->where('project_id', $project_id)
            ->exists();
    }

    public function addItemToCart(Request $request)
    {
        // Validate the inputs
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|integer',
                'project_id' => 'required|integer',
                'units_sponsored' => 'required|integer',
                ]
        );

        // Throw error if validation fails
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 400);
        }

        // Check for existing item in the Cart Table
        $project_id = $request->post('project_id');
        $user_id = $request->post('user_id');
        $units_sponsored = $request->post('units_sponsored');
        $item_exist = $this->itemExistInCart($user_id, $project_id);
        if (!$item_exist) {
            // Add New item in the cart
            $addtocart = Cart::create(
                [
                    'user_id' => $user_id,
                    'project_id' => $project_id,
                    'units_sponsored' => $units_sponsored,
                    ]
            );
            // If successfully added to cart, send success feedback
            if ($addtocart) {
                return response()->json(['message' => 'Item added to cart successfully.'], 200);
            } else {
                // Otherwise, send failure message
                return response()->json(['message' => 'Item could not be added to cart'], 400);
            }

        } else {
            return response()->json(['message' => 'Item already added to cart.'], 200);
        }

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCartWithDetails($user_id)
    {
        $cart_items = Cart::where('user_id', $user_id)->latest()->get();

        if (count($cart_items) > 0) {
            // Get Farm Details with the cart
            foreach ($cart_items as $cart_item) {
                $cart_item->project_details = Project::where('id', $cart_item->project_id)->first();
                // return project type name too.
            }

            return response()->json(
                [
                    'data' => $cart_items,
                    'count' => sizeof($cart_items),
                    ], 200);
        } else {
            return response()->json(['message' => "There is no cart data for the given user ID"], 404);
        }

    }

    public function updateItemUnits(Request $request)
    {
        // Validate the inputs
        $validator = Validator::make($request->all(), [
            'item_id' => 'required|integer',
            'units_sponsored' => 'required|integer'
        ]);

        // Throw error if validation fails
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 400);
        }

        $item_id = $request->post('item_id');
        if(!is_null(Cart::where('id', $item_id)->first())) {
            $units_sponsored = $request->post('units_sponsored');
            $update = Cart::where('id', $item_id)->update(['units_sponsored' => $units_sponsored]);
            return response()->json(
                ['message' => $update],
                200
            );
        } else {
            return response()->json(
                ['message' => 'Item does not exist in Cart'],
                200
            );
        }

    }

    /**
     * @param int $item_id
     * @return mixed
     */
    public function deleteCartItem(Request $request)
    {
        // Validate the inputs
        $validator = Validator::make($request->all(), [
            'item_id' => 'required|integer',
        ]);

        // Throw error if validation fails
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 400);
        }

        $delete_status = Cart::where('id', $request->post(item_id))->delete();
        if($delete_status) {
            return response()->json(
                ['deleted' => $delete_status,],
                200);
        } else {
            return response()->json(
                ['message' => 'Unsuccessful. Please check your internet connection.'],
                500);

        }

    }

    public function clearCart(Request $request)
    {
        // Validate the inputs
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|integer'
        ]);

        // Throw error if validation fails
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 400);
        }

        $user_id = $request->post('user_id');

        // Clear all user items from the cart
        $cartCleared = Cart::where(['user_id' => $user_id])->delete();

        return response()->json(['message' => 'User cart has been cleared.'], 200);
    }

}
