<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\Project as ProjectResource;
use Illuminate\Http\Request;

use App\User;
use App\Models\Sponsorship;
use Illuminate\Support\Facades\Validator;



class SponsorshipsController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     */

    public function index(Request $request, int $user_id = null)
    {
        if(!is_null($user_id)) {

            $sponsorships = Sponsorship::where('user_id', $user_id)->orderBy('id', 'DESC')->get();
            if(sizeof($sponsorships) > 0) {
                return response()->json(
                    [
                        'data' => $sponsorships,
                        'count' => sizeof($sponsorships),
                        'message' => 'Sponsorships retrieved successfully.',
                    ],
                    200);
            } else {
                return response()->json(['message' => 'No sponsorship by this user.'], 404);
            }
        } else {
            $sponsorships = Sponsorship::all();
            return response()->json(
                [
                    'data' => $sponsorships,
                    'count' => sizeof($sponsorships),
                    'message' => 'Sponsorships retrieved successfully.',
                ],
                200);
        }


//        return $this->sendResponse(ProjectResource::collection($sponsorships), 'Sponsorships retrieved successfully.');
    }

    public function show($id)
    {
        return response()->json(
            [
                'message'=> 'Sponsorship successfully returned',
                'sponsorship' => Sponsorship::findOrFail($id),],
            200
        );
    }
}
