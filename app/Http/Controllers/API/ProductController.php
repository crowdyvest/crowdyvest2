<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function index(int $id = null)
    {
        if (!is_null($id)) {
            $product = Product::where('id', $id)->get();
            if(sizeof($product) == 0) {
                return response()->json(['message' => 'No Product found with the ID '. $id], 400);
            } else {
                return response()->json(['data' => $product], 200);
            }
        } else {
            $products = Product::all();
            if(!is_null($products)) {
                return response()->json(['data' => $products], 200);
            }
        }
    }

    public function createProduct(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'name' => ['required', 'string',],
            'description' => ['required', 'string',],
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $product = Product::create([
            'name' => $request->post('name'),
            'description' => $request->post('description'),
        ]);

        if($product) {
            return response()->json(['message' => 'Product created successfully.'], 200);
        } else {
            return response()->json(['error' => 'Failed to create Product, check internet connection.'], 500);
        }
    }

    public function updateProduct(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'id' => ['required', 'integer'],
            'name' => ['required', 'string',],
            'description' => ['required', 'string',],
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $updateProduct = Product::find($request->post('id'));

        $updateProduct->name = $request->post('name');
        $updateProduct->description = $request->post('description');

        $updateProduct->save();

        if($updateProduct) {
            return response()->json(['message' => 'Product updated successfully. '], 200);
        } else {
            return response()->json(['error' => 'Failed to update product. check internet connection'], 500);
        }

    }

    public function deleteProduct($id)
    {
        $deleteProduct = Product::find($id);
        if(is_null($deleteProduct)) {
            return response()->json(['error' => 'No Product found with the ID '. $id], 400);
        } else {
            $deleteProduct->delete();
            return response()->json(['message' => 'Product deleted successfully.'], 200);
        }


    }
}
