<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Status;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    public function index(int $id = null)
    {
        if($id) {
            $statuses = Status::findOrFail($id);
            if(sizeof($statuses) > 0) {
                return response()->json(['data' => $statuses,], 200);
            } else {
                return response()->json(['message' => 'No status found with the ID '.$id], 200);
            }
        } else {
            return response()->json(['data' => Status::all(), ], 200);
        }
    }
}
