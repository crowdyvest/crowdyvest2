<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * creates a user's access token
     * @param $UserModel
     * @return string
     */
    public static function createAccessToken($UserModel) {
        $accessToken = '';
        try{
            if($token = $UserModel->createToken('accessToken')->accessToken){
                $accessToken = $token;
            }

        }catch (Exception $e){return $accessToken = '';}
        return $accessToken;
    }

    /**
     * validates an associative array empty fields
     * @param array $fields
     * @return string
     */
    public static function checkEmptyFields(array $fields){
        $result = '';
        if(count($fields) > 0){
            foreach($fields as $k => $v){
                if(empty($v) || !isset($v) || is_null($v)){
                    $result = "The $k field is required.";
                    return $result;
                }
            }
        }

        return $result;
    }

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public static function sendResponse($result, $message)
    {
        $response = [
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];


        return response()->json($response, 200);
    }


    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public static function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $error,
        ];


        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }


        return response()->json($response, $code);
    }


}
