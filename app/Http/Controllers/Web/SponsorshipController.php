<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\ProjectType;
use App\Models\Status;
use Illuminate\Http\Request;
use App\models\Sponsorship;
use App\Models\Project;


class SponsorshipController extends Controller
{
    public function sponsorshop(Request $request) {
//            $user = Auth::user();
//            $request->session()->put('sortBy', $request->get('sortBy') );

            $page   = (int)$request->get('page');
            $per_page = 12;
            $offset = $page * $per_page;

            $sortBy = request()->session()->get('sortBy');
            $projectType = ProjectType::pluck('name', 'id')->all();
            if(!empty($sortBy)) {
                $projects = Project::where(['publish' => 1, 'company_id' => $sortBy ])
                    ->orderBy('created_at', 'DESC')
                    ->skip($offset)
                    ->take($per_page)
                    ->paginate(9);
            } else {
                $projects = Project::where('publish', 1)
                    ->orderBy('created_at', 'DESC')
                    ->skip($offset)
                    ->take($per_page)
                    ->paginate(9);
            }
            $show_pagination = true;
            $statustype = Status::pluck('name','id')->all();
            $pg = "fm";
//            dd($projects);
            return view(
                'cvhome.sponsorshop',
                compact(
                    'projects',
                    'show_pagination',
                    'sortBy',
                    'projectType',
                    'statustype',
                    'pg'
                )
            )->render();

    }
}
