<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Savings;

class SavingsController extends Controller
{
    public function index()
    {
        return view('cvusers.savings')->render();
    }
}
