<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\ProjectType;
use Illuminate\Http\Request;


use App\Models\Project;
use Illuminate\Support\Facades\DB;

class ProjectsController extends Controller
{
    public function show($project_id) {
        $project = Project::where('slug', $project_id)->first();
        dd($project);
    }

    public function projectsDetails($projectObject) {
        $projects = [];
        foreach ($projectObject as $project) {
            $singleProjectDetail = [
                'title' =>  ProjectsController::getProjectTypeDetails($project->project_type_id)->name,
                'percentage' => ProjectsController::getProjectROI($project->id),
                'cycle' => $project->cycle,
                'price' => $project->price_per_unit,
                'company' => ProjectsController::projectCompany($project->company_id),
                'image' => $project->image,
                'slug' => $project->slug,
            ];
            array_push($projects, $singleProjectDetail);
        }

        return $projects;
    }

    public function getProjectTypeDetails($project_type_id) {
        return ProjectType::where('id', $project_type_id)->first();
    }

    public function getProjectROI($project_id) {
        return DB::table('roi')->where('project_id', $project_id)->first()->percentage;
    }

    public function projectCompany($company_id) {
        return DB::table('companies')->where('id', $company_id)->first();
    }
}
