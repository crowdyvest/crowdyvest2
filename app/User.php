<?php

namespace App;

use App\Models\NextOfKin;
use App\Models\Profile;
use App\Models\Status;
use App\Models\UserBank;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Support\Str;
use Laravel\Passport\HasApiTokens;

use Illuminate\Support\Facades\DB;

// use App\Http\Controllers\Controller;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'phone', 'designation', 'consent', 'role_id', 'status_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the role record associated with the user.
     */
    public function role()
    {
        return $this->hasOne('App\Role');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function designation()
    {
        return $this->hasOne('App\Designation');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function group()
    {
        return $this->hasMany('App\Group');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function next_of_kin() {
        return $this->hasMany(NextOfKin::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status(){
        return $this->hasOne(Status::class, 'status_id', 'id');
    }

    /**
     * Get the Profile record associated with the user.
     */
    public function profile()
    {
        return $this->hasOne(Profile::class, 'user_id', 'id');
    }



    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userbanks() {
        return $this->hasMany(UserBank::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function social_media() {
        return $this->hasMany(UserSocialMedia::class);
    }

    /**
     * @return bool
     */
    public function _profile_is_complete() {
        // put the conditions for checking if a profile is complete here
        return true;
    }

    /**
     * @param $users
     */
    public static  function theLoop($users) {
        foreach ($users as $user) {
            DB::table('users')->insert(
                [
                    'id' => $user["id"],
                    'role_id' => $user['role_id'],
                    'email' => $user['email'],
                    'password' => $user['password'],
                    'api_token' => Str::random(80),
                    'status_id' => $user["status"] == '0' ? 5 : $user["status"],
                    'remember_token' => $user['remember_token'],
                    'created_at' => $user['created_at'],
                    'updated_at' => $user['updated_at'],
                    'verified' => $user["verified"] == '0' ? 5 : $user["verified"],
                    'designation' => $user["designation"] == '0' ? 5 : $user["designation"],
                    'first_name' => $user['first_name'],
                    'last_name' => $user['last_name'],
                    'phone' => $user['phone_number'],
                    'consent' => $user["consent"] == '0' ? 5 : $user["consent"]
                ]
            );
        }
    }

     /**
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getOldUsers() {

        User::on('fcdb')
            ->where('email', '!=', null)
            ->where('role_id', '=', 1)
            ->orderBy('id')
            ->chunk(
                100, function ($users) {
                        self::theLoop($users);
                    }
                );

        User::on('fcdb')
            ->where('email', '!=', null)
            ->where('role_id', '!=', 1)
            ->orderBy('id')
            ->chunk(
                100,
                function ($users) {
                    self::theLoop($users);
                });
    }

     /**
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder[]|\Illuminate\Support\Collection
     */
    private static function duplicatePhoneRecords()
    {
        $duplicateRecords = User::on('fcdb')->select('phone_number')
            ->selectRaw('count(`phone_number`) as `occurrences`')
            /* ->from('users')*/
            ->groupBy('phone_number')
            ->having('occurrences', '>', 1)
            ->get();

        return ($duplicateRecords);
    }

    /**
     * @return mixed
     */
    static function getDuplicatePhoneRecords()
    {
        $duplicateRecords = User::select('phone_number')
            ->selectRaw('count(`phone_number`) as `occurrences`')
            ->groupBy('phone_number')
            ->having('occurrences', '>', 1)
            ->get();

        return ($duplicateRecords);
    }

    /**
     * @return bool
     */
    public static function deleteRecordsWithDuplicatePhoneNumbersAndRetainTheLastOne()
    {
        while ((count(self::duplicatePhoneRecords()))){
            foreach (self::duplicatePhoneRecords() as $record){
                // var_dump($record->phone_number);
                self::deleteUserByPhone($record->phone_number);
            }
        }
        return true;
    }

    /**
     * @param int $phone
     * @return mixed
     */
    private static function deleteUserByPhone($phone){
        return User::on('fcdb')->where('phone_number', $phone)->orderBy('id')->limit(1)->delete();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getOldUsersWithNoPhoneNumber() {
        return User::on('fcdb')
            ->select('id', 'first_name', 'email', 'created_at')->where('role_id', '!=', '1')
            ->where('email', '!=', null)->where('phone_number', null)->get();
    }

    /**
     * @param string $where
     * @param string $value
     * @return mixed
     */
    public static function getPhone(string $where, string $value){
        return self::where($where, $value)->select('phone_number')->first();
    }

    public function propix(int $id) {
        return Profile::where('user_id', $id)->select('propix')->get()->propix;
    }

    /**
     * save the generated profile image to database
     * @param $usrID
     * @param $img_url
     */
    public static function saveProfilePix($usrID, $img_url){
        $User = self::find($usrID);
        if($User){
            //update profile avatar
            $User->profile_image = $img_url;
            $User->save();
        }
    }

    /**
     * @param $phone
     * @return mixed
     */
    public static function activateUser($phone)
    {
        $user = self::where('phone', $phone)->first();
        $user->is_phone_verified = 1;

        //UserCode::invalidateUserCode($user->id);

        return $user->save();

    }

    /**
     * compare a string password with a hashed password
     * @param string $inputPassword
     * @param string $dbPassword
     * @return bool
     */
    public static function comparePasswords(string $inputPassword, string $dbPassword){
        $status = false;
        if (Hash::check($inputPassword, $dbPassword)) $status = true;

        return $status;
    }

    /**
     * this sets all user's is_default_pass_changed to Yes
     * @return \Illuminate\Http\JsonResponse
     */
    public static function setDefaultPasswordChangesToYes(){
        $Users = self::where('is_default_pass_changed', 'No')->get();
        if(count($Users) > 0){
            foreach ($Users as $user) {
                $user->is_default_pass_changed = "Yes";
                $user->save();
            }
        }

        return jsonResponse(true, "Success", 200, count($Users)." records affected.");
    }

    /**
     * change a singular user's is_default_pass_changed to Yes
     * @param int $user_id
     */
    public static function activateIsDefaultPasswordChanged(int $user_id){
        $User = self::where('is_default_pass_changed', 'No')->where('id', $user_id)->first();
        if($User){
            $User->is_default_pass_changed = 'Yes';
            $User->save();
        }
    }


    /**
     * @param $user_id
     * @return mixed
     */
    public static function userExists($user_id) {
        return User::where('id', $user_id)->get();
    }


}
